package me.mathiaseklund.community.commands;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import me.mathiaseklund.community.Main;
import me.mathiaseklund.community.mysql.MySQL;
import me.mathiaseklund.community.utils.Util;

public class LevelTopCommand implements CommandExecutor {

	Main main = Main.getMain();
	Util util = main.getUtil();

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] arg3) {
		ResultSet rs = MySQL.query("SELECT name,level FROM users ORDER BY level DESC LIMIT 10");
		try {
			int count = 0;
			while (rs.next()) {
				count++;
				String name = rs.getString(1);
				int level = rs.getInt(2);
				util.message(sender, "(" + count + ") - " + name + ": " + level);

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

}
