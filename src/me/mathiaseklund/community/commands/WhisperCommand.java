package me.mathiaseklund.community.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.community.Main;
import me.mathiaseklund.community.users.User;
import me.mathiaseklund.community.utils.Util;

public class WhisperCommand implements CommandExecutor {
	Main main = Main.getMain();
	Util util = main.getUtil();

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				if (sender instanceof Player) {
					Player player = (Player) sender;
					User user = main.getUserManager().getUser(player);
					if (args.length > 1) {
						String name = args[0];
						if (!name.equalsIgnoreCase(player.getName())) {
							Player p = Bukkit.getPlayer(name);
							if (p != null) {
								User target = main.getUserManager().getUser(p);
								String message = null;
								for (int i = 0; i < args.length; i++) {
									if (i > 0) {
										if (message == null) {
											message = args[i];
										} else {
											message = message + " " + args[i];
										}
									}
								}
								if (message != null) {
									user.sendWhisper(target, message);
								} else {
									util.message(player, "&7Usage:&e /w <target> <message>");
								}
							} else {
								util.message(sender, "&4ERROR:&7 Target player not found.");
							}
						} else {
							util.message(sender, "&4ERROR:&7 You can't whisper yourself.");
						}
					} else {
						util.message(player, "&7Usage:&e /w <target> <message>");
					}
				}
			}
		});

		return false;
	}

}
