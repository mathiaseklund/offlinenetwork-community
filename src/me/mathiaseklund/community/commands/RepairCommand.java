package me.mathiaseklund.community.commands;

import java.util.Calendar;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.Damageable;
import org.bukkit.inventory.meta.ItemMeta;

import me.mathiaseklund.community.Main;
import me.mathiaseklund.community.users.User;
import me.mathiaseklund.community.utils.Util;

public class RepairCommand implements CommandExecutor {

	Main main = Main.getMain();
	Util util = main.getUtil();

	HashMap<String, Long> cooldown = new HashMap<String, Long>();

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] arg3) {
		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				if (sender instanceof Player) {
					Player player = (Player) sender;
					User user = main.getUserManager().getUser(player);
					int rank = user.getRank();
					int req = main.getConfig().getInt("repair.rank");
					if (rank >= req) {
						if (checkCooldown(player)) {
							ItemStack is = player.getInventory().getItemInMainHand();
							if (is == null || is.getType() == Material.AIR) {
								is = player.getInventory().getItemInOffHand();
							}
							if (is != null) {
								if (is.getItemMeta() instanceof Damageable) {
									// TODO Add cooldown.
									Damageable m = (Damageable) is.getItemMeta();
									m.setDamage(0);
									is.setItemMeta((ItemMeta) m);
									util.message(sender, "&aSuccessfully Repaired Item");
									long now = Calendar.getInstance().getTimeInMillis();
									long cd = 60 * (60 * 1000);
									long until = cd + now;
									cooldown.put(player.getName(), until);
								} else {
									util.message(sender, "&4ERROR:&7 Item can't be repaired.");
								}
							} else {
								util.message(sender, "&4ERROR:&7 You need to hold an item in any of your hands.");
							}
						} else {
							util.message(sender, "&4ERROR:&7 You can't use the repair function yet.");
						}
					} else {
						util.debug(player.getName() + " tried to use the /repair command. Requires permission level: "
								+ req);
						util.message(sender, "&4ERROR:&7 You don't have permission to use this command.");
					}
				}
			}
		});

		return false;
	}

	public boolean checkCooldown(Player player) {
		String name = player.getName();
		boolean b = true;
		if (cooldown.containsKey(name)) {
			long until = cooldown.get(name);
			long now = Calendar.getInstance().getTimeInMillis();
			if (now < until) {
				b = false;
			} else {
				cooldown.remove(name);
			}
		}
		return b;
	}
}
