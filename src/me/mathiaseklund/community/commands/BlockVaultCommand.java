package me.mathiaseklund.community.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.community.Main;
import me.mathiaseklund.community.users.User;
import me.mathiaseklund.community.utils.Util;

public class BlockVaultCommand implements CommandExecutor {

	Main main = Main.getMain();
	Util util = main.getUtil();

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				if (sender instanceof Player) {
					Player player = (Player) sender;

					User user = main.getUserManager().getUser(player);
					int rank = user.getRank();
					if (rank >= 1) {
						if (args.length == 0) {
							util.message(sender,
									"&e/bv <block> &f-&7 Open the block vault for the requested block if available.");
						} else {
							String mat = getVault(args[0]);
							if (mat != null) {
								main.getMiscManager().openBlockVault(player, mat);
							} else {
								util.message(player, "&4ERROR:&7 Block is not available in a BlockVault!");
							}
						}
					}

				} else {
					util.error("Command can only be run by a player.");
				}
			}
		});
		return false;
	}

	String getVault(String mat) {
		if (mat.equalsIgnoreCase("stone")) {
			mat = "STONE";
		} else if (mat.equalsIgnoreCase("dirt")) {
			mat = "DIRT";
		} else if (mat.equalsIgnoreCase("granite")) {
			mat = "GRANITE";
		} else if (mat.equalsIgnoreCase("diorite")) {
			mat = "DIORITE";
		} else if (mat.equalsIgnoreCase("andesite")) {
			mat = "ANDESITE";
		} else if (mat.equalsIgnoreCase("cobblestone") || mat.equalsIgnoreCase("cobble")) {
			mat = "COBBLESTONE";
		} else if (mat.equalsIgnoreCase("oakplank") || mat.equalsIgnoreCase("oakplanks")
				|| mat.equalsIgnoreCase("oak_planks")) {
			mat = "OAK_PLANKS";
		} else if (mat.equalsIgnoreCase("spruceplank") || mat.equalsIgnoreCase("spruceplanks")
				|| mat.equalsIgnoreCase("spruce_planks")) {
			mat = "SPRUCE_PLANKS";
		} else if (mat.equalsIgnoreCase("birchplank") || mat.equalsIgnoreCase("birchplanks")
				|| mat.equalsIgnoreCase("birch_planks")) {
			mat = "BIRCH_PLANKS";
		} else if (mat.equalsIgnoreCase("jungleplank") || mat.equalsIgnoreCase("jungleplanks")
				|| mat.equalsIgnoreCase("jungle_planks")) {
			mat = "JUNGLE_PLANKS";
		} else if (mat.equalsIgnoreCase("acaciaplank") || mat.equalsIgnoreCase("acaciaplanks")
				|| mat.equalsIgnoreCase("acacia_planks")) {
			mat = "ACACIA_PLANKS";
		} else if (mat.equalsIgnoreCase("darkoakplank") || mat.equalsIgnoreCase("darkoakplanks")
				|| mat.equalsIgnoreCase("dark_oak_planks")) {
			mat = "DARK_OAK_PLANKS";
		} else if (mat.equalsIgnoreCase("sand")) {
			mat = "SAND";
		} else if (mat.equalsIgnoreCase("redsand") || mat.equalsIgnoreCase("red_sand")) {
			mat = "RED_SAND";
		} else if (mat.equalsIgnoreCase("gravel")) {
			mat = "GRAVEL";
		} else if (mat.equalsIgnoreCase("oaklog") || mat.equalsIgnoreCase("oaklog")
				|| mat.equalsIgnoreCase("oak_log")) {
			mat = "OAK_LOG";
		} else if (mat.equalsIgnoreCase("sprucelog") || mat.equalsIgnoreCase("sprucelog")
				|| mat.equalsIgnoreCase("spruce_log")) {
			mat = "SPRUCE_LOG";
		} else if (mat.equalsIgnoreCase("birchlog") || mat.equalsIgnoreCase("birchlog")
				|| mat.equalsIgnoreCase("birch_log")) {
			mat = "BIRCH_LOG";
		} else if (mat.equalsIgnoreCase("junglelog") || mat.equalsIgnoreCase("junglelog")
				|| mat.equalsIgnoreCase("jungle_log")) {
			mat = "JUNGLE_LOG";
		} else if (mat.equalsIgnoreCase("acacialog") || mat.equalsIgnoreCase("acacialog")
				|| mat.equalsIgnoreCase("acacia_log")) {
			mat = "ACACIA_LOG";
		} else if (mat.equalsIgnoreCase("darkoaklog") || mat.equalsIgnoreCase("darkoaklog")
				|| mat.equalsIgnoreCase("dark_oak_log")) {
			mat = "DARK_OAK_LOG";
		} else if (mat.equalsIgnoreCase("strippedoak") || mat.equalsIgnoreCase("strippedoaklog")
				|| mat.equalsIgnoreCase("stripped_oak_log")) {
			mat = "STRIPPED_OAK_LOG";
		} else if (mat.equalsIgnoreCase("strippedspruce") || mat.equalsIgnoreCase("strippedsprucelog")
				|| mat.equalsIgnoreCase("stripped_spruce_log")) {
			mat = "STRIPPED_SPRUCE_LOG";
		} else if (mat.equalsIgnoreCase("strippedbirch") || mat.equalsIgnoreCase("strippedbirchlog")
				|| mat.equalsIgnoreCase("stripped_birch_log")) {
			mat = "STRIPPED_BIRCH_LOG";
		} else if (mat.equalsIgnoreCase("strippedjungle") || mat.equalsIgnoreCase("strippedjunglelog")
				|| mat.equalsIgnoreCase("stripped_jungle_log")) {
			mat = "STRIPPED_JUNGLE_LOG";
		} else if (mat.equalsIgnoreCase("strippedacacia") || mat.equalsIgnoreCase("strippedacacialog")
				|| mat.equalsIgnoreCase("stripped_acacia_log")) {
			mat = "STRIPPED_ACACIA_LOG";
		} else if (mat.equalsIgnoreCase("strippeddarkoak") || mat.equalsIgnoreCase("strippeddarkoaklog")
				|| mat.equalsIgnoreCase("stripped_dark_oak_log")) {
			mat = "STRIPPED_DARK_OAK_LOG";
		} else if (mat.equalsIgnoreCase("oakwood") || mat.equalsIgnoreCase("oak_wood")) {
			mat = "OAK_WOOD";
		} else if (mat.equalsIgnoreCase("sprucewood") || mat.equalsIgnoreCase("spruce_wood")) {
			mat = "SPRUCE_WOOD";
		} else if (mat.equalsIgnoreCase("birchwood") || mat.equalsIgnoreCase("birch_wood")) {
			mat = "BIRCH_WOOD";
		} else if (mat.equalsIgnoreCase("junglewood") || mat.equalsIgnoreCase("jungle_wood")) {
			mat = "JUNGLE_WOOD";
		} else if (mat.equalsIgnoreCase("acaciawood") || mat.equalsIgnoreCase("acacia_wood")) {
			mat = "ACACIA_WOOD";
		} else if (mat.equalsIgnoreCase("darkoakwood") || mat.equalsIgnoreCase("dark_oak_wood")) {
			mat = "DARK_OAK_WOOD";
		} else if (mat.equalsIgnoreCase("glass")) {
			mat = "GLASS";
		} else if (mat.equalsIgnoreCase("sandstone")) {
			mat = "SANDSTONE";
		} else if (mat.equalsIgnoreCase("cutsandstone") || mat.equalsIgnoreCase("cutsand")) {
			mat = "CUT_SANDSTONE";
		} else if (mat.equalsIgnoreCase("chiseledsandstone") || mat.equalsIgnoreCase("chiseledsand")) {
			mat = "CHISELED_SANDSTONE";
		} else if (mat.equalsIgnoreCase("whitewool") || mat.equalsIgnoreCase("white_wool")) {
			mat = "WHITE_WOOL";
		} else if (mat.equalsIgnoreCase("orangewool") || mat.equalsIgnoreCase("orange_wool")) {
			mat = "ORANGE_WOOL";
		} else if (mat.equalsIgnoreCase("magentawool") || mat.equalsIgnoreCase("magenta_wool")) {
			mat = "MAGENTA_WOOL";
		} else if (mat.equalsIgnoreCase("lightbluewool") || mat.equalsIgnoreCase("lightblue_wool")
				|| mat.equalsIgnoreCase("light_blue_wool")) {
			mat = "LIGHT_BLUE_WOOL";
		} else if (mat.equalsIgnoreCase("yellowwool") || mat.equalsIgnoreCase("yellow_wool")) {
			mat = "YELLOW_WOOL";
		} else if (mat.equalsIgnoreCase("limewool") || mat.equalsIgnoreCase("lime_wool")) {
			mat = "LIME_WOOL";
		} else if (mat.equalsIgnoreCase("pinkwool") || mat.equalsIgnoreCase("pink_wool")) {
			mat = "PINK_WOOL";
		} else if (mat.equalsIgnoreCase("graywool") || mat.equalsIgnoreCase("gray_wool")) {
			mat = "GRAY_WOOL";
		} else if (mat.equalsIgnoreCase("lightgraywool") || mat.equalsIgnoreCase("lightgray_wool")
				|| mat.equalsIgnoreCase("light_gray_wool")) {
			mat = "LIGHT_GRAY_WOOL";
		} else if (mat.equalsIgnoreCase("cyanwool") || mat.equalsIgnoreCase("cyan_wool")) {
			mat = "CYAN_WOOL";
		} else if (mat.equalsIgnoreCase("purplewool") || mat.equalsIgnoreCase("purple_wool")) {
			mat = "PURPLE_WOOL";
		} else if (mat.equalsIgnoreCase("bluewool") || mat.equalsIgnoreCase("blue_wool")) {
			mat = "BLUE_WOOL";
		} else if (mat.equalsIgnoreCase("brownwool") || mat.equalsIgnoreCase("brown_wool")) {
			mat = "BROWN_WOOL";
		} else if (mat.equalsIgnoreCase("greenwool") || mat.equalsIgnoreCase("green_wool")) {
			mat = "GREEN_WOOL";
		} else if (mat.equalsIgnoreCase("redwool") || mat.equalsIgnoreCase("red_wool")) {
			mat = "RED_WOOL";
		} else if (mat.equalsIgnoreCase("blackwool") || mat.equalsIgnoreCase("black_wool")) {
			mat = "BLACK_WOOL";
		} else if (mat.equalsIgnoreCase("oakslab") || mat.equalsIgnoreCase("oak_slab")) {
			mat = "OAK_SLAB";
		} else if (mat.equalsIgnoreCase("spruceslab") || mat.equalsIgnoreCase("spruce_slab")) {
			mat = "SPRUCE_SLAB";
		} else if (mat.equalsIgnoreCase("birchslab") || mat.equalsIgnoreCase("birch_slab")) {
			mat = "BIRCH_SLAB";
		} else if (mat.equalsIgnoreCase("jungleslab") || mat.equalsIgnoreCase("jungle_slab")) {
			mat = "JUNGLE_SLAB";
		} else if (mat.equalsIgnoreCase("acaciaslab") || mat.equalsIgnoreCase("acacia_slab")) {
			mat = "ACACIA_SLAB";
		} else if (mat.equalsIgnoreCase("darkoakslab") || mat.equalsIgnoreCase("darkoak_slab")) {
			mat = "DARK_OAK_SLAB";
		} else if (mat.equalsIgnoreCase("stoneslab") || mat.equalsIgnoreCase("stone_slab")) {
			mat = "STONE_SLAB";
		} else if (mat.equalsIgnoreCase("smoothstoneslab") || mat.equalsIgnoreCase("smoothstone_slab")) {
			mat = "SMOOTH_STONE_SLAB";
		} else if (mat.equalsIgnoreCase("sandstoneslab") || mat.equalsIgnoreCase("sandstone_slab")) {
			mat = "SANDSTONE_SLAB";
		} else if (mat.equalsIgnoreCase("cutsandstoneslab") || mat.equalsIgnoreCase("cutsandstone_slab")) {
			mat = "CUT_SANDSTONE_SLAB";
		} else if (mat.equalsIgnoreCase("cobblestoneslab") || mat.equalsIgnoreCase("cobblestone_slab")) {
			mat = "COBBLESTONE_SLAB";
		} else if (mat.equalsIgnoreCase("brickslab") || mat.equalsIgnoreCase("brick_slab")) {
			mat = "BRICK_SLAB";
		} else if (mat.equalsIgnoreCase("stonebrickslab") || mat.equalsIgnoreCase("stonebrick_slab")) {
			mat = "STONE_BRICK_SLAB";
		} else if (mat.equalsIgnoreCase("netherbrickslab") || mat.equalsIgnoreCase("netherbrick_slab")) {
			mat = "NETHER_BRICK_SLAB";
		} else if (mat.equalsIgnoreCase("quartzslab") || mat.equalsIgnoreCase("quartz_slab")) {
			mat = "QUARTZ_SLAB";
		} else if (mat.equalsIgnoreCase("redsandstoneslab") || mat.equalsIgnoreCase("redsandstone_slab")) {
			mat = "RED_SANDSTONE_SLAB";
		} else if (mat.equalsIgnoreCase("cutredsandstoneslab") || mat.equalsIgnoreCase("cutredsandstone_slab")) {
			mat = "CUT_RED_SANDSTONE_SLAB";
		} else if (mat.equalsIgnoreCase("purpurslab") || mat.equalsIgnoreCase("purpur_slab")) {
			mat = "PURPUR_SLAB";
		} else if (mat.equalsIgnoreCase("prismarineslab") || mat.equalsIgnoreCase("prismarine_slab")) {
			mat = "PRISMARINE_SLAB";
		} else if (mat.equalsIgnoreCase("prismarinebrickslab") || mat.equalsIgnoreCase("prismarinebrick_slab")) {
			mat = "PRISMARINE_BRICK_SLAB";
		} else if (mat.equalsIgnoreCase("darkprismarineslab") || mat.equalsIgnoreCase("darkprismarine_slab")) {
			mat = "DARK_PRISMARINE_SLAB";
		} else if (mat.equalsIgnoreCase("smoothquartz")) {
			mat = "SMOOTH_QUARTZ";
		} else if (mat.equalsIgnoreCase("smoothredsandstone")) {
			mat = "SMOOTH_RED_SANDSTONE";
		} else if (mat.equalsIgnoreCase("")) {
			mat = "SMOOTH_SANDSTONE";
		} else if (mat.equalsIgnoreCase("")) {
			mat = "SMOOTH_STONE";
		} else if (mat.equalsIgnoreCase("")) {
			mat = "BRICKS";
		} else if (mat.equalsIgnoreCase("")) {
			mat = "BOOKSHELF";
		} else if (mat.equalsIgnoreCase("")) {
			mat = "MOSSY_COBBLESTONE";
		} else if (mat.equalsIgnoreCase("")) {
			mat = "OBSIDIAN";
		} else if (mat.equalsIgnoreCase("")) {
			mat = "PURPUR_BLOCK";
		} else if (mat.equalsIgnoreCase("")) {
			mat = "PURPUR_PILLAR";
		} else if (mat.equalsIgnoreCase("")) {
			mat = "PURPUR_STAIRS";
		} else if (mat.equalsIgnoreCase("")) {
			mat = "OAK_STAIRS";
		} else if (mat.equalsIgnoreCase("")) {
			mat = "COBBLESTONE_STAIRS";
		} else if (mat.equalsIgnoreCase("")) {
			mat = "ICE";
		} else if (mat.equalsIgnoreCase("")) {
			mat = "SNOW_BLOCK";
		} else if (mat.equalsIgnoreCase("")) {
			mat = "CLAY";
		} else if (mat.equalsIgnoreCase("")) {
			mat = "PUMPKIN";
		} else if (mat.equalsIgnoreCase("")) {
			mat = "NETHERRACK";
		} else if (mat.equalsIgnoreCase("")) {
			mat = "SOUL_SAND";
		} else if (mat.equalsIgnoreCase("")) {
			mat = "GLOWSTONE";
		} else if (mat.equalsIgnoreCase("")) {
			mat = "STONE_BRICKS";
		} else if (mat.equalsIgnoreCase("")) {
			mat = "MOSSY_STONE_BRICKS";
		} else if (mat.equalsIgnoreCase("")) {
			mat = "CRACKED_STONE_BRICKS";
		} else if (mat.equalsIgnoreCase("")) {
			mat = "CHISELED_STONE_BRICKS";
		} else if (mat.equalsIgnoreCase("")) {
			mat = "MELON";
		} else if (mat.equalsIgnoreCase("")) {
			mat = "SPRUCE_STAIRS";
		} else if (mat.equalsIgnoreCase("")) {
			mat = "BIRCH_STAIRS";
		} else if (mat.equalsIgnoreCase("")) {
			mat = "JUNGLE_STAIRS";
		} else if (mat.equalsIgnoreCase("")) {
			mat = "CHISELED_QUARTZ_BLOCK";
		} else if (mat.equalsIgnoreCase("")) {
			mat = "QUARTZ_BLOCK";
		} else if (mat.equalsIgnoreCase("")) {
			mat = "QUARTZ_PILLAR";
		} else if (mat.equalsIgnoreCase("")) {
			mat = "QUARTZ_STAIRS";
		} else if (mat.equalsIgnoreCase("")) {
			mat = "WHITE_TERRACOTTA";
		} else if (mat.equalsIgnoreCase("")) {
			mat = "ORANGE_TERRACOTTA";
		} else if (mat.equalsIgnoreCase("")) {
			mat = "MAGENTA_TERRACOTTA";
		} else if (mat.equalsIgnoreCase("")) {
			mat = "LIGHT_BLUE_TERRACOTTA";
		} else if (mat.equalsIgnoreCase("")) {
			mat = "YELLOW_TERRACOTTA";
		} else if (mat.equalsIgnoreCase("")) {
			mat = "";
		} else {
			mat = null;
		}
		util.debug("mat: " + mat);
		return mat;
	}
}
