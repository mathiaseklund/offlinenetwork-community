package me.mathiaseklund.community.commands;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import me.mathiaseklund.community.Main;
import me.mathiaseklund.community.users.User;
import me.mathiaseklund.community.utils.Util;

public class ItemCommand implements CommandExecutor {

	Main main = Main.getMain();
	Util util = main.getUtil();

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				boolean b = false;
				Player player = null;
				if (sender.isOp()) {
					b = true;
				}

				if (sender instanceof Player) {
					player = (Player) sender;
					User user = main.getUserManager().getUser(player);
					if (user.getRank() >= 2) {
						if (!b) {
							b = true;
						}
					} else {
						b = false;
					}
				}

				if (b) {
					if (args.length == 0) {
						// TODO Item GUI
						util.message(sender, "&e/item <id> <amount>");
					} else if (args.length >= 1) {
						String id = args[0];
						if (main.getItemManager().itemExists(id)) {
							ItemStack is = main.getItemManager().getItem(id).getItemStack();
							if (args.length > 1) {
								is.setAmount(Integer.parseInt(args[1]));
							}
							if (is != null) {
								player.getInventory().addItem(is);
							} else {
								util.error("CustomItem ItemStack is NULL");
							}
						} else {
							Material mat = Material.matchMaterial(id);
							if (mat != null) {
								ItemStack is = new ItemStack(mat);
								if (args.length > 1) {
									is.setAmount(Integer.parseInt(args[1]));
								}
								player.getInventory().addItem(is);
							} else {
								util.message(sender, "&4ERROR:&7 Item not found.");
							}
						}
					}
				}
			}
		});

		return false;
	}

}
