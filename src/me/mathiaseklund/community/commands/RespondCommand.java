package me.mathiaseklund.community.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.community.Main;
import me.mathiaseklund.community.users.User;
import me.mathiaseklund.community.utils.Util;

public class RespondCommand implements CommandExecutor {

	Main main = Main.getMain();
	Util util = main.getUtil();

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				if (sender instanceof Player) {
					Player player = (Player) sender;
					if (args.length > 0) {
						User user = main.getUserManager().getUser(player);
						String message = null;
						for (int i = 0; i < args.length; i++) {
							if (message == null) {
								message = args[i];
							} else {
								message = message + " " + args[i];
							}
						}
						if (message != null) {
							user.sendResponse(message);
						} else {
							util.message(sender, "&7Usage:&e /r <message>");
						}
					} else {
						util.message(sender, "&7Usage:&e /r <message>");
					}
				}		
			}
		});
		
		return false;
	}

}
