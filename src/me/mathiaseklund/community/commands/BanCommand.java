package me.mathiaseklund.community.commands;

import java.util.Calendar;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.community.Main;
import me.mathiaseklund.community.mysql.SQL;
import me.mathiaseklund.community.users.User;
import me.mathiaseklund.community.utils.Util;

public class BanCommand implements CommandExecutor {

	Main main = Main.getMain();
	Util util = main.getUtil();

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				boolean canuse = false;
				if (sender instanceof Player) {
					Player player = (Player) sender;
					User user = main.getUserManager().getUser(player);
					int rank = user.getRank();
					if (rank >= 2) {
						canuse = true;
					}
				} else {
					canuse = true;
				}

				if (canuse) {
					if (args.length != 3) {
						util.message(sender, "&e/ban <target> <time> <perm?(y/n)>");
					} else {
						Player player = Bukkit.getPlayer(args[0]);
						if (player != null) {

							User user = main.getUserManager().getUser(player);
							String ip = user.getIp();

							long time = Integer.parseInt(args[1]);
							time = time * 60;
							time = time * 1000;
							long now = Calendar.getInstance().getTimeInMillis();

							long until = now + time;

							int perm = 0;
							if (args[2].equalsIgnoreCase("y")) {
								perm = 1;
							}

							SQL.insertData("ip, until, perm", "'" + ip + "'," + until + "," + perm + "", "banned");
							SQL.insertData("ip, until, perm", "'" + player.getName() + "'," + until + "," + perm + "", "banned");
							Bukkit.getScheduler().runTask(main, new Runnable() {
								public void run() {
									player.kickPlayer("You've been banned from the server!");
								}
							});

						} else {
							util.message(sender, "&4ERROR:&7 Unable to find target player.");
						}
					}
				}
			}
		});

		return false;
	}

}
