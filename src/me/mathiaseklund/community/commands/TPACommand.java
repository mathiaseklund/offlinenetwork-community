package me.mathiaseklund.community.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.community.Main;
import me.mathiaseklund.community.tp.TeleportManager;
import me.mathiaseklund.community.utils.Util;

public class TPACommand implements CommandExecutor {

	Main main = Main.getMain();
	Util util = main.getUtil();

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				if (sender instanceof Player) {
					Player player = (Player) sender;
					if (args.length == 0) {
						util.message(sender, "&e/tpa <target> &f-&7 Send a teleport requeste to target player.");
					} else {
						TeleportManager manager = main.getTeleportManager();
						Player target = Bukkit.getPlayer(args[0]);
						if (target != null) {
							manager.sendTeleportRequest(player, target);
						} else {
							util.message(sender, "&4ERROR:&7 Target player not found.");
						}
					}
				} else {
					util.error("Command can only be run by a player.");
				}
			}
		});

		return false;
	}

}
