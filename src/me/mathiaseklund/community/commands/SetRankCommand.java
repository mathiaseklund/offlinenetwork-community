package me.mathiaseklund.community.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.community.Main;
import me.mathiaseklund.community.users.User;
import me.mathiaseklund.community.utils.Util;

public class SetRankCommand implements CommandExecutor {

	Main main = Main.getMain();
	Util util = main.getUtil();

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				if (sender instanceof Player) {
					if (sender.isOp()) {
						util.message(sender, "&4ERROR:&7 This command can only be run by the console.");
					}
				} else {
					if (args.length == 2) {
						String name = args[0];
						int rank = Integer.parseInt(args[1]);
						Player player = (Player) Bukkit.getPlayer(name);
						User user = null;
						if (player == null) {
							user = main.getUserManager().getUser(name);
						} else {
							user = main.getUserManager().getUser(player);
						}

						if (user != null) {
							if (user.getRank() != rank) {
								user.setRank(rank);
							} else {
								util.message(sender, "&4ERROR:&7 User is already requested rank.");
							}
						} else {
							util.message(sender, "&4ERROR:&7 No user found with specified name.");
						}
					}
				}
			}
		});

		return false;
	}

}
