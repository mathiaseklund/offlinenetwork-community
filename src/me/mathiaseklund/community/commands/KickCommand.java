package me.mathiaseklund.community.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.community.Main;
import me.mathiaseklund.community.users.User;
import me.mathiaseklund.community.utils.Util;

public class KickCommand implements CommandExecutor {
	Main main = Main.getMain();
	Util util = main.getUtil();

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				boolean b = false;
				if (sender instanceof Player) {
					Player player = (Player) sender;
					User user = main.getUserManager().getUser(player);
					int rank = user.getRank();
					if (rank >= 2) {
						b = true;
					}
				} else {
					b = true;
				}

				if (b) {
					if (args.length == 1) {
						Player player = Bukkit.getPlayer(args[0]);
						if (player != null) {
							Bukkit.getScheduler().runTask(main, new Runnable() {
								public void run() {
									player.kickPlayer(
											"You've been kicked from the server by " + sender.getName() + "!");
								}
							});
						} else {
							util.message(sender, "&4ERROR:&7 Unable to find target player.");
						}
					} else {
						util.message(sender, "&e/kick <target> &f-&7 Kick target player.");
					}
				}
			}
		});

		return false;
	}

}
