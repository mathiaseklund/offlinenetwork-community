package me.mathiaseklund.community.commands;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.community.Main;
import me.mathiaseklund.community.users.User;
import me.mathiaseklund.community.utils.Util;

public class FlyCommand implements CommandExecutor {

	Main main = Main.getMain();

	Util util = main.getUtil();

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] arg3) {
		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				if (sender instanceof Player) {
					Player player = (Player) sender;
					User user = main.getUserManager().getUser(player);
					int rank = user.getRank();
					if (rank >= 1) {
						// player is premium or staff
						Location loc = player.getLocation();

						// check if location is within a claim
						String coord = main.getClaimManager().getChunkCoord(loc);
						if (main.getClaimManager().isClaimed(coord)) {
							String owner = main.getClaimManager().getClaimOwner(coord);
							if (owner.equals(player.getName())) {
								// player owns location
								player.setAllowFlight(!player.getAllowFlight());
								util.message(player, "&6Fly Mode toggled!");
							} else {
								util.message(sender, "&4ERROR:&7 You can't use fly in someone elses claim.");
							}
						} else {
							util.message(sender,
									"&4ERROR:&7 You need to be inside your claimed area to use this command.");
						}
					}
				}
			}
		});

		return false;
	}

}
