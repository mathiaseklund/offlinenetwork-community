package me.mathiaseklund.community.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.community.Main;
import me.mathiaseklund.community.users.User;
import me.mathiaseklund.community.utils.Util;

public class AutoPickupCommand implements CommandExecutor {

	Main main = Main.getMain();
	Util util = main.getUtil();

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] arg3) {
		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				if (sender instanceof Player) {
					Player player = (Player) sender;
					User user = main.getUserManager().getUser(player);
					if (user.getRank() >= 1) {
						boolean b = user.getAutoPickup();
						if (b) {
							b = false;
						} else {
							b = true;
						}
						user.setAutoPickup(b);

						if (b) {
							util.message(sender, "&eAuto Pickup has been &aEnabled&e!");
						} else {
							util.message(sender, "&eAuto Pickup has been &cDisabled&e!");
						}
					}
				}
			}
		});

		return false;
	}

}
