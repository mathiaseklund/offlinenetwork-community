package me.mathiaseklund.community.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.community.Main;
import me.mathiaseklund.community.users.User;
import me.mathiaseklund.community.utils.Util;

public class FeedCommand implements CommandExecutor {

	Main main = Main.getMain();
	Util util = main.getUtil();

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] arg3) {
		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				if (sender instanceof Player) {
					Player player = (Player) sender;
					User user = main.getUserManager().getUser(player);
					int rank = user.getRank();
					int req = main.getConfig().getInt("feed.rank");
					if (rank >= req) {

						// TODO Add cooldown
						player.setFoodLevel(20);
						util.message(sender, "&6You've been fed!");
					} else {
						util.debug(player.getName() + " tried to use the /feed command. Requires permission level: "
								+ req);
						util.message(sender, "&4ERROR:&7 You don't have permission to use this command.");
					}
				}
			}
		});

		return false;
	}

}
