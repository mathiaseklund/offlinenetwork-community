package me.mathiaseklund.community.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.community.Main;
import me.mathiaseklund.community.users.User;
import me.mathiaseklund.community.utils.Util;

public class CraftCommand implements CommandExecutor {
	Main main = Main.getMain();
	Util util = main.getUtil();

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] arg3) {
		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				if (sender instanceof Player) {
					Player player = (Player) sender;
					User user = main.getUserManager().getUser(player);
					if (user.getRank() >= 1) {
						Bukkit.getScheduler().runTask(main, new Runnable() {
							public void run() {
								player.openWorkbench(null, true);
							}
						});

					} else {
						util.message(sender, "&4ERROR:&7 This command can only be used by &6Premium Users&7!");
					}
				}
			}
		});

		return false;
	}

}
