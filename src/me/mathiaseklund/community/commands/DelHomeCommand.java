package me.mathiaseklund.community.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.community.Main;
import me.mathiaseklund.community.utils.Util;

public class DelHomeCommand implements CommandExecutor {

	Main main = Main.getMain();
	Util util = main.getUtil();

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				if (sender instanceof Player) {
					Player player = (Player) sender;
					if (args.length == 0) {
						util.message(sender, "&e/delhome <Home#> &f-&7 Delete a home you've previously set.");
					} else {
						if (util.isInteger(args[0])) {
							int index = Integer.parseInt(args[0]);
							main.getMiscManager().delHome(player, index);
						}
					}
				} else {
					util.error("Command can only be run by a player.");
				}
			}
		});

		return false;
	}

}
