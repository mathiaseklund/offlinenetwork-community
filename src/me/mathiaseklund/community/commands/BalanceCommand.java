package me.mathiaseklund.community.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.community.Main;
import me.mathiaseklund.community.users.User;
import me.mathiaseklund.community.utils.Util;

public class BalanceCommand implements CommandExecutor {

	Main main = Main.getMain();
	Util util = main.getUtil();

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				if (args.length == 0) {
					if (sender instanceof Player) {
						Player player = (Player) sender;
						User user = main.getUserManager().getUser(player);
						double balance = user.getBalance();
						util.message(sender,
								"&eYou currently have &6$" + util.formatMoney(balance) + " &ein your account");
					} else {
						util.warn("Command can only be run by a player.");
					}
				} else {
					boolean b = false;
					if (sender.isOp()) {
						b = true;
					} else {
						if (sender instanceof Player) {
							Player player = (Player) sender;
							User user = main.getUserManager().getUser(player);
							if (user.getRank() == 2) {
								b = true;
							}
						}
					}

					if (b) {
						if (args[0].equalsIgnoreCase("set")) {
							if (args.length == 3) {
								String name = args[1];
								Player player = Bukkit.getPlayer(name);
								if (player != null) {
									double d = Double.parseDouble(args[2]);
									User user = main.getUserManager().getUser(player);
									user.setBalance(d);
									util.message(sender,
											"&aSet " + player.getName() + "'s balance to: &6$" + util.formatMoney(d));
									// util.message(player, "&eYour account balance was set to &6$" +
									// util.formatMoney(d) + "&e!");
								} else {
									util.message(sender, "&4ERROR:&7 Target player not found.");
								}
							} else {
								util.message(sender, "&e/bal set <target> <double>");
							}
						} else if (args[0].equalsIgnoreCase("add")) {
							if (args.length == 3) {
								String name = args[1];
								Player player = Bukkit.getPlayer(name);
								if (player != null) {
									double d = Double.parseDouble(args[2]);
									User user = main.getUserManager().getUser(player);
									user.addBalance(d);
									util.message(player,
											"&6$" + util.formatMoney(d) + "&e was deposited into your account!");
									util.message(player, "&eYou now have &6$" + util.formatMoney(user.getBalance())
											+ "&e in your account.");
									util.message(sender, "&aYou've deposited &6$" + util.formatMoney(d) + "&a into &e"
											+ player.getName() + "&a's account!");
								} else {
									util.message(sender, "&4ERROR:&7 Target player not found.");
								}
							} else {
								util.message(sender, "&e/bal add <target> <double>");
							}
						}
					}
				}
			}
		});

		return false;
	}

}
