package me.mathiaseklund.community.commands;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.community.Main;
import me.mathiaseklund.community.users.User;
import me.mathiaseklund.community.utils.Util;

public class GMSCommand implements CommandExecutor {
	Main main = Main.getMain();
	Util util = main.getUtil();

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] arg3) {
		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				if (sender instanceof Player) {
					Player player = (Player) sender;
					User user = main.getUserManager().getUser(player);
					int rank = user.getRank();
					int req = main.getConfig().getInt("gms.rank");
					if (rank >= req) {
						if (player.getGameMode() != GameMode.SURVIVAL) {
							Bukkit.getScheduler().runTask(main, new Runnable() {
								public void run() {
									player.setGameMode(GameMode.SURVIVAL);
								}
							});

							util.message(sender, "&7You are now in Survival Mode!");
						} else {
							util.message(sender, "&4ERROR:&7 You are already in Survival Mode");
						}
					} else {
						util.debug(
								player.getName() + " tried to use the /gms command. Requires permission level: " + req);
						util.message(sender, "&4ERROR:&7 You don't have permission to use this command.");
					}
				}
			}
		});

		return false;
	}

}
