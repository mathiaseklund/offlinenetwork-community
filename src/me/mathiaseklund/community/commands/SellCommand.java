package me.mathiaseklund.community.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import me.mathiaseklund.community.Main;
import me.mathiaseklund.community.users.User;
import me.mathiaseklund.community.utils.Util;

public class SellCommand implements CommandExecutor {
	Main main = Main.getMain();
	Util util = main.getUtil();

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				if (sender instanceof Player) {
					Player player = (Player) sender;
					User user = main.getUserManager().getUser(player);
					int rank = user.getRank();
					if (rank >= 0) {
						if (args.length == 0) { // sell item in hand
							ItemStack is = player.getInventory().getItemInMainHand();
							if (is != null) {
								main.getSellManager().attemptSellItemInHand(player, is);
							} else {
								util.message(sender, "&4ERROR:&7 You need to hold an item in your hand to use this command.");
							}
						} else {
							if (args[0].equalsIgnoreCase("set")) { // Set the price of item in hand.
								if (rank >= 2) {
									if (args.length == 2) {
										double price = Double.parseDouble(args[1]);
										ItemStack is = player.getInventory().getItemInMainHand();
										if (is != null) {
											String material = is.getType().toString();
											main.getSellManager().setSellPrice(material, price);
											util.message(sender,
													"&aSet sell price of " + material + " to " + util.formatMoney(price));
										}
									}
								}
							} else if (args[0].equalsIgnoreCase("all")) {
								if (rank >= 1) {
									main.getSellManager().attemptSellAllItems(player);
								} else {
									util.message(sender, "&4ERROR:&7 This command can only be run by &6Premium Users&7!");
								}
							}
						}
					} else {
						util.message(sender, "&4ERROR:&7 You do not have permission to use this command.");
					}
				}		
			}
		});
		
		return false;
	}

}
