package me.mathiaseklund.community.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.community.Main;
import me.mathiaseklund.community.tp.TeleportManager;
import me.mathiaseklund.community.utils.Util;

public class TPAcceptCommand implements CommandExecutor {

	Main main = Main.getMain();
	Util util = main.getUtil();

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				if (sender instanceof Player) {
					Player player = (Player) sender;
					if (args.length == 0) {
						util.message(sender,
								"&e/tpaccept <target> &f-&7 Accept a incoming teleport request from target player.");
					} else {
						Player target = Bukkit.getPlayer(args[0]);
						if (target != null) {
							TeleportManager manager = main.getTeleportManager();
							manager.acceptTeleportRequest(player, target);
						} else {
							util.message(sender, "&4ERROR:&7 Unable to find target player.");
						}
					}
				} else {
					util.error("Command can only be run by a player");
				}
			}
		});

		return false;
	}

}
