package me.mathiaseklund.community.commands;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.community.Main;
import me.mathiaseklund.community.users.User;
import me.mathiaseklund.community.utils.Util;

public class ClaimCommand implements CommandExecutor {

	Main main = Main.getMain();
	Util util = main.getUtil();

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				if (sender instanceof Player) {
					Player player = (Player) sender;
					if (args.length < 1) {
						main.getClaimManager().attemptClaim(player);
					} else {
						if (args[0].equalsIgnoreCase("add")) {
							if (args.length == 2) {
								String name = args[1];

								main.getClaimManager().addClaimAccess(player, name);
								util.message(sender, "&7" + name + "&a has been given access to your claimed areas.");

							} else {
								util.message(sender,
										"&7Usage:&e /claim add <playername> &8-&f Give a player access to your claimed areas.");
							}
						} else if (args[0].equalsIgnoreCase("remove")) {
							if (args.length == 2) {
								String name = args[1];

								main.getClaimManager().removeClaimAccess(player, name);
								util.message(sender, "&7" + name + "&c access to your claimed areas has been revoked.");

							} else {
								util.message(sender,
										"&7Usage:&e /claim remove <playername> &8-&f Remove a players access to your claimed areas.");
							}
						} else if (args[0].equalsIgnoreCase("list")) {
							// list all players with access to your claimed areas.

							User user = main.getUserManager().getUser(player);
							List<String> members = user.getClaimMembers();
							util.message(sender, "&eClaim Members: &a" + members);

						} else {
							util.message(sender,
									"&7Usage:&e /claim add <playername> &8-&f Give a player access to your claimed areas.");
							util.message(sender,
									"&7Usage:&e /claim remove <playername> &8-&f Remove a players access to your claimed areas.");
						}
					}
				}
			}
		});

		return false;
	}

}
