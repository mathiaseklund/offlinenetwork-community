package me.mathiaseklund.community.mysql;

import me.mathiaseklund.community.Main;

public class Config {
	private static final String host = "host";
	private static final String user = "user";
	private static final String password = "password";
	private static final String database = "database";
	private static final String port = "port";

	public static void clear() {
		Config.set(host, "", false);
		Config.set(user, "", false);
		Config.set(password, "", false);
		Config.set(database, "", false);
		Config.set(port, "3306", false);
	}

	public static void create() {
		Config.set(host, "", true);
		Config.set(user, "", true);
		Config.set(password, "", true);
		Config.set(database, "", true);
		Config.set(port, "3306", true);
	}

	public static void reload() {
		Main.getMain().reloadConfig();
		Config.create();
	}

	public static void setHost(String s) {
		if (!Config.getHost().equalsIgnoreCase(s)) {
			Config.set(host, s, false);
		}
	}

	public static void setUser(String s) {
		if (!Config.getUser().equalsIgnoreCase(s)) {
			Config.set(user, s, false);
		}
	}

	public static void setPassword(String s) {
		if (!Config.getPassword().equalsIgnoreCase(s)) {
			Config.set(password, s, false);
		}
	}

	public static void setDatabase(String s) {
		if (!Config.getDatabase().equalsIgnoreCase(s)) {
			Config.set(database, s, false);
		}
	}

	public static void setPort(String s) {
		if (!Config.getPort().equalsIgnoreCase(s)) {
			Config.set(port, s, false);
		}
	}

	public static String getHost() {
		return Config.get(host);
	}

	public static String getUser() {
		return Config.get(user);
	}

	public static String getPassword() {
		return Config.get(password);
	}

	public static String getDatabase() {
		return Config.get(database);
	}

	public static String getPort() {
		return Config.get(port);
	}

	private static void set(String name, Object value, boolean checkIfExists) {
		if (name == null || value == null || checkIfExists && Main.getMain().getConfig().contains(name)) {
			return;
		}
		Main.getMain().getConfig().set(name, value);
		Main.getMain().saveConfig();
	}

	private static String get(String name) {
		return name == null || !Main.getMain().getConfig().contains(name) ? ""
				: Main.getMain().getConfig().getString(name);
	}
}
