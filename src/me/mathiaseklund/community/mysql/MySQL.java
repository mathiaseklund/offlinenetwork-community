package me.mathiaseklund.community.mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

public class MySQL {
	private static Connection con;

	public static Connection getConnection() {
		return con;
	}

	public static void setConnection(String host, String user, String password, String database, String port) {
		if (host == null || user == null || password == null || database == null) {
			return;
		}
		MySQL.disconnect(false);
		try {
			con = DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + database, user, password);
			Bukkit.getConsoleSender().sendMessage((Object) ChatColor.GREEN + "MySQL connected.");
		} catch (Exception e) {
			Bukkit.getConsoleSender().sendMessage((Object) ChatColor.RED + "MySQL Connect Error: " + e.getMessage());
		}
	}

	public static void connect() {
		MySQL.connect(true);
	}

	private static void connect(boolean message) {
		String host = Config.getHost();
		String user = Config.getUser();
		String password = Config.getPassword();
		String database = Config.getDatabase();
		String port = Config.getPort();
		if (MySQL.isConnected()) {
			if (message) {
				Bukkit.getConsoleSender()
						.sendMessage((Object) ChatColor.RED + "MySQL Connect Error: Already connected");
			}
		} else if (host.equalsIgnoreCase("")) {
			Bukkit.getConsoleSender().sendMessage((Object) ChatColor.RED + "Config Error: Host is blank");
		} else if (user.equalsIgnoreCase("")) {
			Bukkit.getConsoleSender().sendMessage((Object) ChatColor.RED + "Config Error: User is blank");
		} else if (password.equalsIgnoreCase("")) {
			Bukkit.getConsoleSender().sendMessage((Object) ChatColor.RED + "Config Error: Password is blank");
		} else if (database.equalsIgnoreCase("")) {
			Bukkit.getConsoleSender().sendMessage((Object) ChatColor.RED + "Config Error: Database is blank");
		} else if (port.equalsIgnoreCase("")) {
			Bukkit.getConsoleSender().sendMessage((Object) ChatColor.RED + "Config Error: Port is blank");
		} else {
			MySQL.setConnection(host, user, password, database, port);
		}
	}

	public static void disconnect() {
		MySQL.disconnect(true);
	}

	private static void disconnect(boolean message) {
		block5: {
			try {
				if (MySQL.isConnected()) {
					con.close();
					Bukkit.getConsoleSender().sendMessage((Object) ChatColor.GREEN + "MySQL disconnected.");
				} else if (message) {
					Bukkit.getConsoleSender()
							.sendMessage((Object) ChatColor.RED + "MySQL Disconnect Error: No existing connection");
				}
			} catch (Exception e) {
				if (!message)
					break block5;
				Bukkit.getConsoleSender()
						.sendMessage((Object) ChatColor.RED + "MySQL Disconnect Error: " + e.getMessage());
			}
		}
		con = null;
	}

	public static void reconnect() {
		MySQL.disconnect();
		MySQL.connect();
	}

	public static boolean isConnected() {
		if (con != null) {
			try {
				return !con.isClosed();
			} catch (Exception e) {
				Bukkit.getConsoleSender().sendMessage((Object) ChatColor.RED + "MySQL Connection:");
				Bukkit.getConsoleSender().sendMessage((Object) ChatColor.RED + "Error: " + e.getMessage());
			}
		}
		return false;
	}

	public static void update(String command) {
		if (command == null) {
			return;
		}
		MySQL.connect(false);
		try {
			Statement st = MySQL.getConnection().createStatement();
			st.executeUpdate(command);
			st.close();
		} catch (Exception e) {
			Bukkit.getConsoleSender().sendMessage((Object) ChatColor.RED + "MySQL Update:");
			Bukkit.getConsoleSender().sendMessage((Object) ChatColor.RED + "Command: " + command);
			Bukkit.getConsoleSender().sendMessage((Object) ChatColor.RED + "Error: " + e.getMessage());
		}
	}

	public static ResultSet query(String command) {
		if (command == null) {
			return null;
		}
		MySQL.connect(false);
		ResultSet rs = null;
		try {
			Statement st = MySQL.getConnection().createStatement();
			rs = st.executeQuery(command);
		} catch (Exception e) {
			Bukkit.getConsoleSender().sendMessage((Object) ChatColor.RED + "MySQL Query:");
			Bukkit.getConsoleSender().sendMessage((Object) ChatColor.RED + "Command: " + command);
			Bukkit.getConsoleSender().sendMessage((Object) ChatColor.RED + "Error: " + e.getMessage());
		}
		return rs;
	}
}
