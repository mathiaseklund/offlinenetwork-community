package me.mathiaseklund.community.mysql;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.community.Main;
import me.mathiaseklund.community.utils.Util;

public class MySQLCommands implements CommandExecutor {

	Main main = Main.getMain();
	Util util = main.getUtil();

	private void send(CommandSender sender) {
		util.message(sender, "&eUsage:&7 /mysql <reload/rl>");
		util.message(sender, "&eUsage:&7 /mysql <connect/disconnect/reconnect>");
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("mysql")) {
			if (!(sender instanceof Player)) {
				if (args.length == 0) {
					send(sender);
				} else if (args.length == 1) {
					String s = args[0];
					if (s.equalsIgnoreCase("reload") || s.equalsIgnoreCase("rl")) {
						Config.reload();
						util.message(sender, "&aMySQL configuration reloaded.");
					} else if (s.equalsIgnoreCase("connect")) {
						util.message(sender, "&aMySQL is trying to connect.");
						MySQL.connect();
					} else if (s.equalsIgnoreCase("disconnect")) {
						util.message(sender, "&aMySQL is trying to disconnect.");
						MySQL.disconnect();
					} else if (s.equalsIgnoreCase("reconnect")) {
						util.message(sender, "&aMySQL is trying to reconnect.");
						MySQL.reconnect();
					} else {
						send(sender);
					}
				} else {
					send(sender);
				}
			}
		}
		return false;
	}
}
