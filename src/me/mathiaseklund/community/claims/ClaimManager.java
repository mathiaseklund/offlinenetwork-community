package me.mathiaseklund.community.claims;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Statistic;
import org.bukkit.entity.Player;

import me.mathiaseklund.community.Main;
import me.mathiaseklund.community.mysql.MySQL;
import me.mathiaseklund.community.mysql.SQL;
import me.mathiaseklund.community.users.User;
import me.mathiaseklund.community.utils.Util;

public class ClaimManager {

	Main main;
	Util util;

	String table = "claims";

	HashMap<String, String> claims;
	List<String> confirming = new ArrayList<String>();

	public ClaimManager() {
		main = Main.getMain();
		util = main.getUtil();
		loadAllClaims();
	}

	void loadAllClaims() {

		util.debug("Loading All Claims");
		claims = new HashMap<String, String>();
		ResultSet rs = MySQL.query("SELECT coord, owner FROM claims");
		try {
			while (rs.next()) {
				String coord = rs.getString(1);
				String owner = rs.getString(2);
				claims.put(coord, owner);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			util.debug("Finished Loading All Claims");
		}

	}

	// METHODS

	public void attemptClaim(Player player) {
		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				String name = player.getName();
				if (!confirming.contains(name)) {
					util.debug(name + " is attempting claim.");
					Location loc = player.getLocation();
					int cx = loc.getChunk().getX();
					int cz = loc.getChunk().getZ();
					String world = loc.getWorld().getName();
					String coord = world + " " + cx + " " + cz;
					if (isClaimed(coord)) {
						String owner = getClaimOwner(coord);
						util.debug("Coordinate is already claimed by " + owner);
						util.message(player, "&cChunk is already claimed by: " + owner);
						util.message(player, "&cPlease try again at another location.");
					} else {
						long playtime = player.getStatistic(Statistic.PLAY_ONE_MINUTE);
						playtime = playtime / 20 / 60 / 60;
						util.debug(name + " playtime: " + playtime + " hours");
						int totalClaimsAvailable = 0;
						if (main.getUserManager().getUser(player).getRank() >= 1) {
							totalClaimsAvailable = (int) (playtime * 2) + 5;
						} else {
							totalClaimsAvailable = (int) (playtime / 2) + 1;
						}
						util.debug("total claims available: " + totalClaimsAvailable);

						ResultSet rs = MySQL.query("SELECT coord FROM " + table + " WHERE owner = '" + name + "';");
						int claims = 0;
						try {
							while (rs.next()) {
								claims++;
							}
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} finally {
							util.debug("Claims: " + claims);
							if (claims < totalClaimsAvailable) {
								util.message(player, "&eChunk Claim Available!");
								util.message(player, "");
								util.message(player, "&eAvailable Claims: &7" + (totalClaimsAvailable - claims));
								util.message(player, "&eType &7/claim&e again to confirm chunk claim.");
								confirming.add(name);
								Bukkit.getScheduler().runTaskLaterAsynchronously(main, new Runnable() {
									public void run() {
										if (confirming.contains(name)) {
											confirming.remove(name);
											util.message(player, "&cClaim Request timed out");
										}
									}
								}, 3 * 20);
							} else {
								util.debug(name + " is not able to claim anymore chunks at this time.");
								util.message(player, "&4ERROR:&7 You are unable to claim any chunks at the moment.");
							}
						}
					}
				} else {
					confirming.remove(name);
					util.debug(name + " is confirming claim");
					Location loc = player.getLocation();
					int cx = loc.getChunk().getX();
					int cz = loc.getChunk().getZ();
					String world = loc.getWorld().getName();
					String coord = world + " " + cx + " " + cz;
					if (!isClaimed(coord)) {
						claim(name, world, cx, cz);
						util.message(player, "&aSuccessfully claimed chunk! &7(&eX:" + cx + " Z:" + cz + "&7)");
					}
				}
			}
		});
	}

	public void attemptUnclaim(Player player) {
		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				Location loc = player.getLocation();
				String coord = getChunkCoord(loc);
				if (isClaimed(coord)) {
					String owner = getClaimOwner(coord);
					if (owner.equals(player.getName())) {
						unclaim(player.getName(), coord);
						util.message(player, "&cYou've unclaimed this chunk!");
					} else {
						util.message(player, "&4ERROR:&7 This claim is not owned by you!");
					}
				} else {
					util.message(player, "&4ERROR:&7 This chunk hasn't been claimed!");
				}
			}
		});

	}

	void unclaim(String name, String coord) {
		claims.remove(coord);
		SQL.deleteData("coord", "=", coord, table);
		util.debug("Unclaimed Chunk");
	}

	void claim(String name, String world, int x, int z) {
		claims.put(world + " " + x + " " + z, name);
		SQL.insertData("coord, owner", "'" + (world + " " + x + " " + z) + "' ,'" + name + "'", table);
		util.debug("Stored claim.");
	}

	public void addClaimAccess(Player player, String name) {
		// TODO
		User user = main.getUserManager().getUser(player);
		user.addClaimMember(name);
	}

	public void removeClaimAccess(Player player, String name) {
		// TODO
		User user = main.getUserManager().getUser(player);
		user.removeClaimMember(name);
	}

	// BOOLEANS

	public boolean isClaimed(String coord) {
		util.debug(coord + " to check");
		return claims.containsKey(coord);
	}

	public boolean isClaimMember(String owner, String name) {
		User user = main.getUserManager().getUser(owner);
		return user.isClaimMember(name);
	}

	// GETTERS

	public String getClaimOwner(String coord) {
		String owner = null;
		if (isClaimed(coord)) {
			owner = claims.get(coord);
		}
		return owner;
	}

	public String getChunkCoord(Location loc) {
		String world = loc.getWorld().getName();
		int x = loc.getChunk().getX();
		int z = loc.getChunk().getZ();
		String coord = world + " " + x + " " + z;
		return coord;
	}
}
