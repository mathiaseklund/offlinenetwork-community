package me.mathiaseklund.community.users;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

import me.mathiaseklund.community.Main;
import me.mathiaseklund.community.items.ItemTextAPI;
import me.mathiaseklund.community.mysql.MySQL;
import me.mathiaseklund.community.mysql.SQL;
import me.mathiaseklund.community.utils.Util;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.TextComponent;

public class User {

	Main main;
	Util util;
	File f;
	FileConfiguration data;
	String table = "users";

	Player player;
	String name, email, sqlId, ip;
	int rankId, level;
	List<String> claimmembers, homes;
	HashMap<String, Integer> bv;
	double balance;

	User lastWhisperer;

	// SETTINGS
	boolean autopickup;
	boolean vanished;

	public User(Player player) {
		this.player = player;
		this.name = player.getName();
		this.main = Main.getMain();
		this.util = main.getUtil();
		load();

	}

	public User(String name) {
		main = Main.getMain();
		util = main.getUtil();
		this.name = name;
		load();
	}

	void load() {
		util.debug("Loading " + name + "'s Data");
		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				fetchSQLData();
				f = new File(main.getDataFolder() + "/users/" + name + ".yml");
				if (!f.exists()) {
					try {
						f.createNewFile();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				data = YamlConfiguration.loadConfiguration(f);

				loadBooleans();
				loadBlockVaults();
			}
		});

	}

	void save() {
		try {
			data.save(f);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	void loadBooleans() {
		autopickup = data.getBoolean("auto-pickup");
		vanished = data.getBoolean("vanished");
	}

	// BOOLEANS

	public boolean isClaimMember(String member) {
		List<String> members = getClaimMembers();
		return members.contains(member);
	}

	public boolean getAutoPickup() {
		boolean b = false;
		if (getRank() >= 1) {
			return autopickup;
		}
		return b;
	}

	public boolean isVanished() {
		return vanished;
	}

	// GETTERS

	public int getRank() {
		return rankId;
	}

	public double getBalance() {
		return balance;
	}

	public List<String> getClaimMembers() {
		if (claimmembers == null) {
			claimmembers = data.getStringList("claim.members");
		}
		return claimmembers;
	}

	public Player getPlayer() {
		return player;
	}

	public String getIp() {
		return ip;
	}

	public List<String> getHomes() {
		if (homes == null) {
			if (data.contains("homes")) {
				homes = data.getStringList("homes");
			} else {
				homes = new ArrayList<String>();
			}
		}
		return homes;
	}

	public String getHome(int index) {
		List<String> homes = getHomes();
		if (homes != null) {
			if (homes.size() > index) {
				String home = homes.get(index);
				if (home != null) {
					return home;
				} else {
					return null;
				}
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	public int getBlockVaultAmount(String mat) {
		int amount = 0;
		if (bv.containsKey(mat)) {
			amount = bv.get(mat);
		}
		return amount;
	}

	public int getLevel() {
		return level;
	}

	// SETTERS

	public void setClaimMembers(List<String> members) {
		claimmembers = members;
		data.set("claim.members", members);
		save();
	}

	public void setBalance(double d) {
		balance = d;
		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				SQL.set("balance", d, "id", "=", sqlId, table);
			}
		});
	}

	public void setRank(int rank) {
		rankId = rank;
		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				SQL.set("rank", rank, "id", "=", sqlId, table);
			}
		});
	}

	public void setAutoPickup(boolean b) {
		autopickup = b;
		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				data.set("auto-pickup", b);
				save();
			}
		});
	}

	@SuppressWarnings("deprecation")
	public void setVanished(boolean b) {
		vanished = b;
		if (b) {
			Bukkit.getScheduler().runTask(main, new Runnable() {
				public void run() {
					for (Player p : Bukkit.getOnlinePlayers()) {
						p.hidePlayer(main, player);
						p.hidePlayer(player);
					}
				}
			});

		} else {
			Bukkit.getScheduler().runTask(main, new Runnable() {
				public void run() {
					for (Player p : Bukkit.getOnlinePlayers()) {
						p.showPlayer(main, player);
						p.showPlayer(player);
					}
				}
			});

		}
		data.set("vanished", b);
		save();

	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public void setHomes(List<String> list) {
		this.homes = list;
		data.set("homes", list);
		save();
	}

	public void setBlockVaultAmount(String mat, int amount) {
		bv.put(mat, amount);
		data.set("blockvault." + mat, amount);
		save();
	}

	public void setLevel(int level) {
		this.level = level;
		MySQL.update("UPDATE users SET level=" + level + " WHERE id=" + sqlId);
	}

	// MODIFIERS

	public void addClaimMember(String member) {
		List<String> members = getClaimMembers();
		if (!members.contains(member)) {
			members.add(member);
			setClaimMembers(members);
		}
	}

	public void removeClaimMember(String member) {
		List<String> members = getClaimMembers();
		if (members.contains(member)) {
			members.remove(member);
			setClaimMembers(members);
		}
	}

	public void addBalance(double d) {
		setBalance(getBalance() + d);
	}

	public void takeBalance(double d) {
		setBalance(getBalance() - d);
	}

	public void addHome(String home) {
		List<String> homes = getHomes();
		homes.add(home);
		setHomes(homes);

	}

	public void delHome(String home) {
		List<String> homes = getHomes();
		if (homes.contains(home)) {
			homes.remove(home);
			setHomes(homes);
		} else {
			util.debug("Can't delete home as it was not found.");
		}
	}

	public void addBlockVaultAmount(String mat, int amount) {
		int bv = getBlockVaultAmount(mat);
		bv = bv + amount;
		setBlockVaultAmount(mat, bv);
	}

	// SQL

	void fetchSQLData() {
		util.debug("Fetching SQL Data");
		if (SQL.exists("name", name, table)) {
			sqlId = SQL.get("id", "name", "=", name, table).toString();
			email = SQL.get("email", "id", "=", sqlId, table).toString();
			rankId = Integer.parseInt(SQL.get("rank", "id", "=", sqlId, table).toString());
			balance = (double) SQL.get("balance", "id", "=", sqlId, table);
			level = (int) SQL.get("level", "id", "=", sqlId, table);
			util.debug("Fetched all data");
		} else {
			sendToAuth();
		}
	}

	void sendToAuth() {
		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		out.writeUTF("Connect");
		out.writeUTF("auth");
		player.sendPluginMessage(main, "BungeeCord", out.toByteArray());

	}

	// METHODS

	public void sendWhisper(User target, String message) {
		target.receiveWhisper(this, message);

		int rank = getRank();
		TextComponent text = new TextComponent(ChatColor.translateAlternateColorCodes('&', "&5[W] "));

		String rankprefix = "";
		if (rank == 0) {
			rankprefix = ChatColor.translateAlternateColorCodes('&', "&7");
		} else if (rank == 1) {
			rankprefix = ChatColor.translateAlternateColorCodes('&', "&8[&6✪&8]&7 ");
		} else if (rank == 2) {
			rankprefix = ChatColor.translateAlternateColorCodes('&', "&8[&4STAFF&8]&7 ");
		} else {
			rankprefix = ChatColor.translateAlternateColorCodes('&', "&7");
		}
		text.addExtra(rankprefix);

		TextComponent name = new TextComponent(ChatColor.translateAlternateColorCodes('&', player.getDisplayName()));
		TextComponent divider = new TextComponent(ChatColor.translateAlternateColorCodes('&',
				"&8 -> &7" + target.getPlayer().getDisplayName() + "&8 > &5"));

		TextComponent item = new TextComponent("");

		if (message.startsWith("[item]")) {
			message = message.replaceAll("\\[item\\]", "");
			ItemStack is = player.getInventory().getItem(player.getInventory().getHeldItemSlot());

			if (is != null) {
				item = ItemTextAPI.getItemComponent(is);
			}
		}

		TextComponent chatmessage = new TextComponent();

		chatmessage.setText(message);

		text.addExtra(name);
		text.addExtra(divider);
		text.addExtra(item);
		text.addExtra(chatmessage);

		player.spigot().sendMessage(text);
		util.message(Bukkit.getConsoleSender(), text.toLegacyText());
	}

	public void receiveWhisper(User sender, String message) {
		this.lastWhisperer = sender;
		int rank = sender.getRank();
		Player player = sender.getPlayer();
		TextComponent text = new TextComponent(ChatColor.translateAlternateColorCodes('&', "&5[W] "));

		String rankprefix = "";
		if (rank == 0) {
			rankprefix = ChatColor.translateAlternateColorCodes('&', "&7");
		} else if (rank == 1) {
			rankprefix = ChatColor.translateAlternateColorCodes('&', "&8[&6✪&8]&7 ");
		} else if (rank == 2) {
			rankprefix = ChatColor.translateAlternateColorCodes('&', "&8[&4STAFF&8]&7 ");
		}
		text.addExtra(rankprefix);

		TextComponent name = new TextComponent(ChatColor.translateAlternateColorCodes('&', player.getDisplayName()));
		TextComponent divider = new TextComponent(ChatColor.translateAlternateColorCodes('&', "&8 > &5"));

		TextComponent item = new TextComponent("");

		if (message.startsWith("[item]")) {
			message = message.replaceAll("\\[item\\]", "");
			ItemStack is = player.getInventory().getItem(player.getInventory().getHeldItemSlot());

			if (is != null) {
				item = ItemTextAPI.getItemComponent(is);
			}
		}

		TextComponent chatmessage = new TextComponent();

		chatmessage.setText(message);

		text.addExtra(name);
		text.addExtra(divider);
		text.addExtra(item);
		text.addExtra(chatmessage);
		getPlayer().spigot().sendMessage(text);
	}

	public void sendResponse(String message) {
		if (lastWhisperer != null) {
			sendWhisper(lastWhisperer, message);
		} else {
			util.message(player, "&4ERROR:&7 No whispers received.");
		}
	}

	public void loadBlockVaults() {
		bv = new HashMap<String, Integer>();
		if (data.contains("blockvault")) {
			ConfigurationSection section = data.getConfigurationSection("blockvault");
			for (String key : section.getKeys(false)) {
				bv.put(key, data.getInt("blockvault." + key));
			}
		}
	}
}
