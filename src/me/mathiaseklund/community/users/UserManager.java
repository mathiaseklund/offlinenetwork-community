package me.mathiaseklund.community.users;

import java.util.Collection;
import java.util.HashMap;

import org.bukkit.entity.Player;

import me.mathiaseklund.community.Main;

public class UserManager {

	Main main;

	HashMap<String, User> users = new HashMap<String, User>();

	public UserManager() {
		main = Main.getMain();
	}

	public User getUser(Player player) {
		if (users.containsKey(player.getName())) {
			return users.get(player.getName());
		} else {
			User user = new User(player);
			users.put(player.getName(), user);
			return user;
		}
	}

	public User getUser(String name) {
		if (users.containsKey(name)) {
			return users.get(name);
		} else {
			User user = new User(name);
			return user;
		}
	}

	public Collection<User> getOnlineUsers() {
		return users.values();
	}
}
