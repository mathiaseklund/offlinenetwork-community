package me.mathiaseklund.community.sell;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import me.mathiaseklund.community.Main;
import me.mathiaseklund.community.users.User;
import me.mathiaseklund.community.utils.Util;

public class SellManager {

	Main main;
	Util util;

	File f;
	FileConfiguration data;

	HashMap<String, Double> sellprice = new HashMap<String, Double>();

	public SellManager() {
		main = Main.getMain();
		util = main.getUtil();
		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				loadSellData();
			}
		});
	}

	void loadSellData() {
		util.info("Loading Sell Data");
		f = new File(main.getDataFolder(), "sell.yml");
		if (!f.exists()) {
			main.saveResource("sell.yml", true);
		}
		data = YamlConfiguration.loadConfiguration(f);

		loadSellPrices();
	}

	void loadSellPrices() {
		sellprice.clear();
		for (String key : data.getConfigurationSection("").getKeys(false)) {
			double price = data.getDouble(key);
			sellprice.put(key, price);
		}
		util.info("Finished Loading Sell Data");
	}

	void save() {
		try {
			data.save(f);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// GETTERS

	public double getSellPrice(String material) {
		double sp = 0;
		if (sellprice.containsKey(material)) {
			sp = sellprice.get(material);
		}
		return sp;
	}

	// SETTERS

	public void setSellPrice(String material, double price) {
		sellprice.put(material, price);
		data.set(material, price);
		save();
	}

	// BOOLEANS

	public boolean canSellItem(String material) {
		return sellprice.containsKey(material);
	}

	// METHODS

	public void attemptSellItemInHand(Player player, ItemStack is) {
		String material = is.getType().toString();
		double price = getSellPrice(material);
		User user = main.getUserManager().getUser(player);
		double pay = price * is.getAmount();
		if (pay > 0) {
			user.addBalance(pay);
			util.message(player,
					"&aYou sold &e" + is.getAmount() + "x " + material + "&a for &6$" + util.formatMoney(pay));
			player.getInventory().setItemInMainHand(null);
		} else {
			util.message(player, "&4ERROR:&7 Unable to sell item.");
		}
	}

	public void attemptSellAllItems(Player player) {
		double totalprice = 0;
		int totalamount = 0;
		for (int i = 0; i < player.getInventory().getSize(); i++) {
			ItemStack is = player.getInventory().getItem(i);
			if (is != null) {
				String material = is.getType().toString();
				if (canSellItem(material)) {
					double price = getSellPrice(material);
					totalprice = totalprice + (price * is.getAmount());
					totalamount = totalamount + is.getAmount();
					player.getInventory().setItem(i, null);
				}
			}
		}
		User user = main.getUserManager().getUser(player);
		user.addBalance(totalprice);
		util.message(player, "&aYou sold &e" + totalamount + "x Items&a for &6$" + util.formatMoney(totalprice));
	}
}
