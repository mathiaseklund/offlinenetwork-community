package me.mathiaseklund.community.hoppers;

import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Hopper;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import me.mathiaseklund.community.Main;
import me.mathiaseklund.community.mysql.MySQL;
import me.mathiaseklund.community.mysql.SQL;
import me.mathiaseklund.community.utils.Util;

public class HopperManager {

	Main main;
	Util util;

	HashMap<String, String> choppers = new HashMap<String, String>();
	HashMap<String, String> chopperlocations = new HashMap<String, String>();
	HashMap<String, Integer> chopperids = new HashMap<String, Integer>();
	HashMap<Integer, File> chopperfiles = new HashMap<Integer, File>();
	HashMap<Integer, FileConfiguration> chopperconfigs = new HashMap<Integer, FileConfiguration>();
	List<String> opened = new ArrayList<String>();
	List<String> sending = new ArrayList<String>();

	public HopperManager() {
		main = Main.getMain();
		util = main.getUtil();
		loadChunkHoppers();
	}

	void loadChunkHoppers() {
		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				util.debug("Loading chunk hoppers");
				ResultSet rs = MySQL.query("SELECT id, coord, owner, location FROM hoppers WHERE type=0;");
				try {
					while (rs.next()) {
						int id = rs.getInt(1);
						String coord = rs.getString(2);
						String owner = rs.getString(3);
						String location = rs.getString(4);
						choppers.put(coord, owner);
						chopperlocations.put(coord, location);
						chopperids.put(coord, id);
						util.debug("Found hopper owned by " + owner + " at (" + coord + ")");
						File f = new File(main.getDataFolder() + "/hoppers/" + id + ".yml");
						if (!f.exists()) {
							try {
								f.createNewFile();
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						FileConfiguration data = YamlConfiguration.loadConfiguration(f);
						if (!data.contains("location")) {
							data.set("location", location);
							try {
								data.save(f);
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}

						chopperfiles.put(id, f);
						chopperconfigs.put(id, data);
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		});
	}

	// BOOLEANS

	public boolean hasChunkHopper(String coord) {
		return choppers.containsKey(coord);
	}

	public boolean hasChunkHopper(Location loc) {
		String world = loc.getWorld().getName();
		int x = loc.getChunk().getX();
		int z = loc.getChunk().getZ();
		String coord = world + " " + x + " " + z;
		return hasChunkHopper(coord);
	}

	// GETTERS

	public String getChunkHopperLocation(String coord) {
		return chopperlocations.get(coord);
	}

	public int getChunkHopperId(String coord) {
		return chopperids.get(coord);
	}

	// METHODS

	public void placeChunkHopper(Player player, Location loc) {
		String world = loc.getWorld().getName();
		int x = loc.getChunk().getX();
		int z = loc.getChunk().getZ();
		String coord = world + " " + x + " " + z;
		String location = world + " " + loc.getBlockX() + " " + loc.getBlockY() + " " + loc.getBlockZ();
		choppers.put(coord, player.getName());
		chopperlocations.put(coord, location);
		SQL.insertData("coord, owner, location", "'" + coord + "','" + player.getName() + "','" + location + "'",
				"hoppers");
		int id = (int) SQL.get("id", "coord", "=", coord, "hoppers");
		chopperids.put(coord, id);
		File f = new File(main.getDataFolder() + "/hoppers/" + id + ".yml");
		if (!f.exists()) {
			try {
				f.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		FileConfiguration data = YamlConfiguration.loadConfiguration(f);
		if (!data.contains("location")) {
			data.set("location", location);
			try {
				data.save(f);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		chopperfiles.put(id, f);
		chopperconfigs.put(id, data);
	}

	public void breakChunkHopper(Player player, Location loc) {
		String world = loc.getWorld().getName();
		int x = loc.getChunk().getX();
		int z = loc.getChunk().getZ();
		String coord = world + " " + x + " " + z;
		int id = chopperids.get(coord);
		File f = chopperfiles.get(id);
		FileConfiguration data = chopperconfigs.get(id);
		for (String s : data.getConfigurationSection("").getKeys(false)) {
			if (!s.equalsIgnoreCase("amount")) {
				ItemStack is = data.getItemStack(s);
				if (is != null) {
					int empty = player.getInventory().firstEmpty();
					if (empty >= 0) {
						player.getInventory().addItem(is);
					} else {
						loc.getWorld().dropItem(loc, is);
					}
				}
			}
		}
		f.delete();
		chopperids.remove(coord);
		chopperfiles.remove(id);
		chopperconfigs.remove(id);
		chopperlocations.remove(coord);
		choppers.remove(coord);
		SQL.deleteData("id", "=", id + "", "hoppers");
		util.info("Chunk Hopper Deleted");
	}

	public void openChunkHopper(Player player, int id) {
		if (!opened.contains(id + "")) {
			FileConfiguration data = chopperconfigs.get(id);
			if (data != null) {
				Inventory inv = Bukkit.createInventory(null, 18,
						util.hide("[ch" + id + "]") + ChatColor.translateAlternateColorCodes('&', "&eChunk Hopper"));
				for (String s : data.getConfigurationSection("").getKeys(false)) {
					if (!s.equalsIgnoreCase("empty")) {
						ItemStack is = data.getItemStack(s);
						if (is != null) {
							inv.addItem(is);
						}
					}
				}
				opened.add(id + "");
				player.openInventory(inv);
			} else {
				util.message(player, "&4ERROR:&7 Error opening chunk hopper.");
			}
		} else {
			util.message(player, "&4ERROR:&7 Someone is already viewing hopper content.");
		}
	}

	public void closeChunkHopper(int id, Inventory inv) {
		if (opened.contains(id + "")) {
			opened.remove(id + "");
		}
		FileConfiguration data = chopperconfigs.get(id);
		String location = data.getString("location");
		ConfigurationSection section = data.getConfigurationSection("");
		if (section != null) {
			for (String s : section.getKeys(false)) {
				data.set(s, null);
			}
		}
		int am = 0;
		for (ItemStack is : inv.getContents()) {
			if (is != null) {
				data.set(am + "", is);
				am++;
			}
		}
		data.set("empty", inv.firstEmpty());
		data.set("location", location);
		try {
			data.save(chopperfiles.get(id));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			util.debug("saved hopper content");
		}
		// sendChunkHopperItemToChest(id);
	}

	public void addItemToChunkHopper(String coord, ItemStack is) {
		String location = getChunkHopperLocation(coord);
		String world = location.split(" ")[0];
		int x = Integer.parseInt(location.split(" ")[1]);
		int y = Integer.parseInt(location.split(" ")[2]);
		int z = Integer.parseInt(location.split(" ")[3]);
		Location loc = new Location(Bukkit.getWorld(world), x, y, z);
		Block block = loc.getBlock();
		if (block.getType() == Material.HOPPER) {
			Hopper hopper = (Hopper) block.getState();
			for (int i = 0; i < hopper.getInventory().getSize(); i++) {
				ItemStack item = hopper.getInventory().getItem(i);
				if (item != null) {
					if (item.isSimilar(is)) {
						int amount = item.getAmount();
						int add = is.getAmount();
						if (amount < 64) {
							if (amount + add < 64) {
								item.setAmount((amount + add));
								hopper.getInventory().setItem(i, item);
								is = null;
								break;
							} else if (amount + add == 64) {
								item.setAmount(64);
								hopper.getInventory().setItem(i, item);
								is = null;
								break;
							} else {
								item.setAmount(64);
								int rem = (amount + add) - 64;
								is.setAmount(rem);
							}
						}
					}
				} else {
					hopper.getInventory().setItem(i, is);
					is = null;
					break;
				}
			}
			if (is != null) {
				ItemStack item = is;
				Bukkit.getScheduler().runTaskLater(main, new Runnable() {
					public void run() {
						addItemToChunkHopper(coord, item);
					}
				}, 60);
			}
		}
	}

	/*
	 * public void addItemToChunkHopper(int id, ItemStack is) { if
	 * (opened.contains(id + "")) { util.debug("Hopper is being viewed.");
	 * Bukkit.getScheduler().runTaskLaterAsynchronously(main, new Runnable() {
	 * public void run() { addItemToChunkHopper(id, is); } }, 20 * 5); } else {
	 * FileConfiguration data = chopperconfigs.get(id); Inventory inv =
	 * Bukkit.createInventory(null, 18); for (String s :
	 * data.getConfigurationSection("").getKeys(false)) { if
	 * (!s.equalsIgnoreCase("empty")) { ItemStack item = data.getItemStack(s); if
	 * (item != null) { inv.addItem(item); } } } int empty = inv.firstEmpty(); if
	 * (empty >= 0) { inv.addItem(is); closeChunkHopper(id, inv); } else {
	 * util.debug("Hopper is full.");
	 * Bukkit.getScheduler().runTaskLaterAsynchronously(main, new Runnable() {
	 * public void run() { addItemToChunkHopper(id, is); } }, 20 * 10); } } }
	 */

	/*
	 * public void sendChunkHopperItemToChest(int id) { if (sending.contains(id +
	 * "")) { Bukkit.getScheduler().runTaskLater(main, new Runnable() { public void
	 * run() { sendChunkHopperItemToChest(id); } }, 60); } else { sending.add(id +
	 * ""); FileConfiguration data = chopperconfigs.get(id); Inventory inv =
	 * Bukkit.createInventory(null, 18); for (String s :
	 * data.getConfigurationSection("").getKeys(false)) { if
	 * (!s.equalsIgnoreCase("empty")) { ItemStack item = data.getItemStack(s); if
	 * (item != null) { inv.addItem(item); } } } ItemStack is = inv.getItem(0); if
	 * (is != null) { String location = data.getString("location"); if (location !=
	 * null) { String world = location.split(" ")[0]; int x =
	 * Integer.parseInt(location.split(" ")[1]); int y =
	 * Integer.parseInt(location.split(" ")[2]); int z =
	 * Integer.parseInt(location.split(" ")[3]); Location loc = new
	 * Location(Bukkit.getWorld(world), x, y, z); Block block =
	 * loc.getBlock().getRelative(0, -1, 0); if (block.getType() == Material.CHEST
	 * || block.getType() == Material.TRAPPED_CHEST) { Chest chest = (Chest)
	 * block.getState(); if (chest.getInventory().firstEmpty() >= 0) {
	 * chest.getInventory().addItem(is); inv.setItem(0, null); closeChunkHopper(id,
	 * inv); } else { Bukkit.getScheduler().runTaskLater(main, new Runnable() {
	 * public void run() { sendChunkHopperItemToChest(id); } }, 60); } }
	 * sending.remove(id + ""); } else {
	 * util.error("Hopper location not found in data file."); sending.remove(id +
	 * ""); } } else { sending.remove(id + ""); } } }
	 */
}
