package me.mathiaseklund.community.listeners;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ItemSpawnEvent;
import org.bukkit.inventory.ItemStack;

import me.mathiaseklund.community.Main;
import me.mathiaseklund.community.utils.Util;

public class ItemListener implements Listener {

	Main main = Main.getMain();
	Util util = main.getUtil();

	ArrayList<String> invFullCD = new ArrayList<String>();

	@EventHandler
	public void onItemSpawn(ItemSpawnEvent event) {
		Item item = event.getEntity();
		ItemStack is = item.getItemStack();
		Location loc = event.getLocation();
		if (main.getHopperManager().hasChunkHopper(loc)) {
			String world = loc.getWorld().getName();
			int x = loc.getChunk().getX();
			int z = loc.getChunk().getZ();
			String coord = world + " " + x + " " + z;
			// int id = main.getHopperManager().getChunkHopperId(coord);
			main.getHopperManager().addItemToChunkHopper(coord, is);
			item.remove();
		}
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onItemSpawn_autopickup(ItemSpawnEvent event) {
		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				Location loc = event.getLocation();
				if (main.getMiscManager().isAPLocation(loc)) {
					String name = main.getMiscManager().getAPLocValue(loc);
					main.getMiscManager().setAPLocValue(loc, null);
					Player player = Bukkit.getPlayer(name);
					ItemStack is = event.getEntity().getItemStack();
					boolean pickedup = false;
					for (int i = 0; i < 35; i++) {
						ItemStack item = player.getInventory().getItem(i);
						if (player.getInventory().getItem(i) != null) {
							if (item.isSimilar(is)) {
								int j = item.getAmount();
								int k = is.getAmount();
								if (j + k < 64) {
									item.setAmount(j + k);
									pickedup = true;
									break;
								} else if (j + k == 64) {
									item.setAmount(64);
									pickedup = true;
								}
							}
						} else {
							player.getInventory().addItem(is);
							pickedup = true;
							break;
						}

					}
					if (pickedup) {
						event.getEntity().remove();
						event.setCancelled(true);
					} else {
						if (!invFullCD.contains(name)) {
							util.message(player, "&cYour Inventory is Full! Unable to automatically pickup item");
							invFullCD.add(name);
							Bukkit.getScheduler().runTaskLater(main, new Runnable() {
								public void run() {
									invFullCD.remove(name);
								}
							}, 40);
						}
					}
				}
			}
		});

	}
}
