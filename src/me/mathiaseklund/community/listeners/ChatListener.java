package me.mathiaseklund.community.listeners;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.inventory.ItemStack;

import me.mathiaseklund.community.Main;
import me.mathiaseklund.community.items.ItemTextAPI;
import me.mathiaseklund.community.users.User;
import me.mathiaseklund.community.utils.Util;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.TextComponent;

public class ChatListener implements Listener {

	Main main = Main.getMain();
	Util util = main.getUtil();

	@EventHandler
	public void onChat(AsyncPlayerChatEvent event) {
		Player player = event.getPlayer();
		String message = event.getMessage();
		event.setCancelled(true);
		User user = main.getUserManager().getUser(player);
		int rank = user.getRank();
		if (main.getAuctionHouse().isAuctioning(player)) {
			if (util.isDouble(message)) {
				double price = Double.parseDouble(message);
				if (price > 0) {
					main.getAuctionHouse().finishAuctionProcess(player, price);
				} else {
					util.message(player, "&4ERROR:&7 Auction price has to be > $0.");
				}
			} else {
				main.getAuctionHouse().cancelAuctionProcess(player);
			}
		} else {
			TextComponent text = new TextComponent();

			String rankprefix = "";
			if (rank == 0) {
				rankprefix = ChatColor.translateAlternateColorCodes('&', "&7");
			} else if (rank == 1) {
				rankprefix = ChatColor.translateAlternateColorCodes('&', "&8[&6✪&8]&7 ");
			} else if (rank == 2) {
				rankprefix = ChatColor.translateAlternateColorCodes('&', "&8[&4STAFF&8]&7 ");
			}
			text.addExtra(rankprefix);

			TextComponent name = new TextComponent(
					ChatColor.translateAlternateColorCodes('&', player.getDisplayName()));
			TextComponent divider = new TextComponent(ChatColor.translateAlternateColorCodes('&', "&8 > &f"));

			TextComponent item = new TextComponent("");

			// TODO
			if (message.startsWith("[item]")) {
				message = message.replaceAll("\\[item\\]", "");
				ItemStack is = player.getInventory().getItem(player.getInventory().getHeldItemSlot());

				if (is != null) {
					item = ItemTextAPI.getItemComponent(is);
				}
			}

			TextComponent chatmessage = new TextComponent();

			if (rank >= 1) {
				message = ChatColor.translateAlternateColorCodes('&', message);
			}
			chatmessage.setText(message);

			text.addExtra(name);
			text.addExtra(divider);
			text.addExtra(item);
			text.addExtra(chatmessage);

			Bukkit.getConsoleSender().sendMessage(text.toLegacyText());
			for (Player p : Bukkit.getOnlinePlayers()) {
				p.spigot().sendMessage(text);
			}
		}
	}
}
