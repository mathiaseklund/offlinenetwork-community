package me.mathiaseklund.community.listeners;

import java.util.Calendar;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import me.mathiaseklund.community.Main;
import me.mathiaseklund.community.items.CustomItem;
import me.mathiaseklund.community.users.User;
import me.mathiaseklund.community.utils.Util;

public class BlockListener implements Listener {

	Main main = Main.getMain();
	Util util = main.getUtil();

	HashMap<String, Long> breaktime = new HashMap<String, Long>();

	@EventHandler(priority = EventPriority.LOWEST)
	public void onBlockBreak_claimcheck(BlockBreakEvent event) {
		Player player = event.getPlayer();
		Location loc = event.getBlock().getLocation();
		int x = loc.getChunk().getX();
		int z = loc.getChunk().getZ();
		String coord = loc.getWorld().getName() + " " + x + " " + z;
		if (main.getUserManager().getUser(player).getRank() < 2) {
			if (main.getClaimManager().isClaimed(coord)) {
				util.debug(player.getName() + " broke block in claimed area.");
				String owner = main.getClaimManager().getClaimOwner(coord);
				if (owner.equals(player.getName())) {
					util.debug(player.getName() + " owns the chunk. Allowing block break.");
				} else {
					// TODO check for claim membership.
					User user = main.getUserManager().getUser(owner);
					if (user.isClaimMember(player.getName())) {
						util.debug(player.getName() + " is a member of the claim. Allowing block break.");
					} else {
						util.debug(player.getName() + " does not own the chunk. Cancelling block break.");
						event.setCancelled(true);
					}
				}
			} else {
				util.debug("are not claimed.");
			}
		} else {
			util.debug("player is staff");
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onBlockPlace_claimcheck(BlockPlaceEvent event) {
		Player player = event.getPlayer();
		Location loc = event.getBlock().getLocation();
		int x = loc.getChunk().getX();
		int z = loc.getChunk().getZ();
		String coord = x + " " + z;
		if (main.getUserManager().getUser(player).getRank() < 2) {
			if (main.getClaimManager().isClaimed(coord)) {
				util.debug(player.getName() + " placed block in claimed area.");
				String owner = main.getClaimManager().getClaimOwner(coord);
				if (owner.equals(player.getName())) {
					util.debug(player.getName() + " owns the chunk. Allowing block place.");
				} else {
					// TODO check for claim membership.
					User user = main.getUserManager().getUser(owner);
					if (user.isClaimMember(player.getName())) {
						util.debug(player.getName() + " is a member of the claim. Allowing block place.");
					} else {
						util.debug(player.getName() + " does not own the chunk. Cancelling block place.");
						event.setCancelled(true);
					}
				}
			}
		}
	}

	@EventHandler
	public void onBlockPlace(BlockPlaceEvent event) {
		if (!event.isCancelled()) {
			Player player = event.getPlayer();
			User user = main.getUserManager().getUser(player);
			ItemStack is = event.getItemInHand();
			String id = main.getItemManager().getItemId(is);
			if (id != null) {
				util.debug(player.getName() + " placed custom block.");

				if (id.equalsIgnoreCase("chopper")) { // Checks if the item is a chunk hopper
					util.debug("placing chunk hopper");
					if (user.getRank() >= 1) {
						util.debug("allowed to place");
						Location loc = event.getBlockPlaced().getLocation();
						if (!main.getHopperManager().hasChunkHopper(loc)) {
							main.getHopperManager().placeChunkHopper(player, loc);
						} else {
							util.message(player, "&4ERROR:&7 Chunk already has a Chunk Hopper!");
							event.setCancelled(true);
						}
					} else {
						util.message(player, "&4ERROR:&7 This block can only be placed by &6Premium Users&7!");
					}
				}
			}
		}
	}

	@EventHandler
	public void onBlockBreak(BlockBreakEvent event) {

		if (!event.isCancelled()) {

			Player player = event.getPlayer();
			Block block = event.getBlock();
			CustomItem ci = main.getItemManager().getItem("chopper");
			if (ci != null) {
				if (block.getType().toString().equalsIgnoreCase(ci.getMaterial())) {
					Location loc = block.getLocation();
					if (main.getHopperManager().hasChunkHopper(loc)) {
						String world = loc.getWorld().getName();
						String location = world + " " + loc.getBlockX() + " " + loc.getBlockY() + " " + loc.getBlockZ();
						String coord = world + " " + loc.getChunk().getX() + " " + loc.getChunk().getZ();
						if (main.getHopperManager().getChunkHopperLocation(coord).equalsIgnoreCase(location)) {
							util.debug("Breaking Chunk Hopper");
							main.getHopperManager().breakChunkHopper(player, loc);
							event.setCancelled(true);
							block.setType(Material.AIR);
							ItemStack is = ci.getItemStack();
							loc.getWorld().dropItem(loc, is);
						}
					}
				}
			}

			util.debug(player.getName() + " finished breaking block.");
			long time = breaktime.get(player.getName());
			long now = Calendar.getInstance().getTimeInMillis();
			long spent = (now - time);
			util.debug("Spent time: " + spent + " MS");

		}
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onBlockBreak_autopickup(BlockBreakEvent event) {
		if (!event.isCancelled()) {
			Player player = event.getPlayer();
			User user = main.getUserManager().getUser(player);
			if (user.getAutoPickup()) {
				Location loc = event.getBlock().getLocation();
				main.getMiscManager().setAPLocValue(loc, player.getName());
			}
		}
	}

	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent event) {
		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				Player player = event.getPlayer();
				if (event.getAction() == Action.LEFT_CLICK_BLOCK) {
					util.debug(player.getName() + " started breaking block.");
					long now = Calendar.getInstance().getTimeInMillis();
					breaktime.put(player.getName(), now);
				}
			}
		});

	}
}
