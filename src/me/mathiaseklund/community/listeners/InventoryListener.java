package me.mathiaseklund.community.listeners;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import me.mathiaseklund.community.Main;
import me.mathiaseklund.community.users.User;
import me.mathiaseklund.community.utils.Util;

public class InventoryListener implements Listener {

	Main main = Main.getMain();
	Util util = main.getUtil();

	@EventHandler
	public void onInventoryClose(InventoryCloseEvent event) {
		String title = util.unhide(event.getView().getTitle());
		if (title.startsWith("[ch")) {
			int id = Integer.parseInt(title.split("\\]")[0].replaceAll("\\[ch", ""));
			main.getHopperManager().closeChunkHopper(id, event.getView().getTopInventory());
		} else if (title.startsWith("[bv]")) {
			if (event.getPlayer() instanceof Player) {
				String mat = title.split("\\]")[1];
				Player player = (Player) event.getPlayer();
				Inventory inv = event.getInventory();
				Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
					public void run() {
						User user = main.getUserManager().getUser(player);
						int amount = 0;
						for (ItemStack is : inv.getContents()) {
							if (is != null) {
								amount = amount + is.getAmount();
							}
						}
						if (amount > 0) {
							user.addBlockVaultAmount(mat, amount);
						}
					}
				});

			}
		}
	}

	@EventHandler
	public void onOpenInventory(InventoryOpenEvent event) {
		if (event.getPlayer() instanceof Player) {
			Player player = (Player) event.getPlayer();
			if (main.getAuctionHouse().isAuctioning(player)) {
				player.closeInventory();
			}
		}
	}

	@EventHandler
	public void onItemHeld(PlayerItemHeldEvent event) {
		Player player = event.getPlayer();
		if (main.getAuctionHouse().isAuctioning(player)) {
			event.setCancelled(true);
		}
	}

	@EventHandler
	public void onInventoryClick(InventoryClickEvent event) {
		if (event.getWhoClicked() instanceof Player) {
			Player player = (Player) event.getWhoClicked();
			String title = event.getView().getTitle();
			title = util.unhide(title);
			if (title.startsWith("[ah]")) {
				event.setCancelled(true);
				ItemStack is = event.getCurrentItem();
				if (is != null) {
					String name = null;
					if (is.hasItemMeta()) {
						if (is.getItemMeta().hasDisplayName()) {
							name = util.unhide(is.getItemMeta().getDisplayName());
						}
					}
					if (name != null) {
						util.debug("item name: " + name);

						if (name.contains("[aid")) {
							name = name.split("\\]")[0];
							int id = Integer.parseInt(name.replaceAll("\\[aid", ""));
							util.debug("Clicked on AuctionID: " + id);

							if (event.getClick() == ClickType.LEFT) {
								util.debug(player.getName() + " wants to purchase the auctioned item.");
								main.getAuctionHouse().attemptPurchaseAuction(player, id);
							}
						}
					} else {
						util.debug("Item name is NULL");
					}
				} else {
					util.debug("Didn't click on anything.");
				}
			} else if (title.startsWith("[bv]")) {
				String mat = title.split("\\]")[1];
				if (event.getClickedInventory().getSize() == 54) {
					ItemStack is = event.getCursor();
					if (is != null) {
						if (is.getType() != Material.AIR) {
							if (!is.getType().toString().equalsIgnoreCase(mat)) {
								util.debug("Clicked with wrong block in hand!");
								event.setCancelled(true);
								return;
							}
						}
					}
					is = event.getCurrentItem();
					if (is != null) {
						if (is.getType() != Material.AIR) {
							if (!is.getType().toString().equalsIgnoreCase(mat)) {
								util.debug("Clicked on wrong block!");
								event.setCancelled(true);
							}
						}
					}
				} else {

					ItemStack is = event.getCurrentItem();
					if (!is.getType().toString().equalsIgnoreCase(mat)) {
						event.setCancelled(true);
					}

				}
			}
		}
	}
}
