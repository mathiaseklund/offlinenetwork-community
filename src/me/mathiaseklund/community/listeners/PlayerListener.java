package me.mathiaseklund.community.listeners;

import java.util.Collection;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.data.Ageable;
import org.bukkit.block.data.BlockData;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLevelChangeEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;

import me.mathiaseklund.community.Main;
import me.mathiaseklund.community.users.User;
import me.mathiaseklund.community.utils.Util;
import net.md_5.bungee.api.ChatColor;

public class PlayerListener implements Listener {

	Main main = Main.getMain();
	Util util = main.getUtil();

	@EventHandler
	public void onPlayerLogin(PlayerLoginEvent event) {
		Player player = event.getPlayer();
		User user = main.getUserManager().getUser(player);
		user.setIp(event.getAddress().toString());
	}

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		event.setJoinMessage(ChatColor.translateAlternateColorCodes('&', "&a+ &7" + player.getName()));
		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				User user = main.getUserManager().getUser(player);
				int rank = user.getRank();

				if (rank < 2) {
					// player is nto staff

					// hide vanished users
					for (User u : main.getUserManager().getOnlineUsers()) {
						if (u.isVanished()) {
							Bukkit.getScheduler().runTaskLater(main, new Runnable() {
								public void run() {
									player.hidePlayer(main, u.getPlayer());
								}
							}, 1);

						}
					}
				} else {
					// player is staff

					if (user.isVanished()) {
						for (Player p : Bukkit.getOnlinePlayers()) {
							Bukkit.getScheduler().runTaskLater(main, new Runnable() {
								public void run() {
									p.hidePlayer(main, player);
								}
							}, 1);
						}
					}
				}
			}
		});

	}

	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event) {
		Player player = event.getPlayer();
		event.setQuitMessage(ChatColor.translateAlternateColorCodes('&', "&c- &f" + player.getName()));
	}

	@EventHandler
	public void onInteract(PlayerInteractEvent event) {
		if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
			if (event.getHand() == EquipmentSlot.HAND) {
				Player player = event.getPlayer();
				Block block = event.getClickedBlock();
				Location loc = block.getLocation();
				// util.debug(player.getName() + " clicked on " + block.getType().toString());
				BlockData bd = block.getBlockData();

				// Check if crop is being right clicked and harvest and replant if player is
				// premium.
				if (bd instanceof Ageable) {
					Ageable crop = (Ageable) bd;
					int age = crop.getAge();
					int maxage = crop.getMaximumAge();
					if (age == maxage) {
						User user = main.getUserManager().getUser(player);
						if (user.getRank() >= 1) {
							event.setCancelled(true);
							Collection<ItemStack> drops = block.getDrops();
							crop.setAge(0);
							block.setBlockData(crop);
							if (user.getAutoPickup()) {
								main.getMiscManager().setAPLocValue(loc, player.getName());
							}
							for (ItemStack is : drops) {
								if (is != null) {
									loc.getWorld().dropItem(loc, is);
								}
							}
						}
					}
				}

				/*
				 * if (block.getType().toString().contains("SIGN")) { if
				 * (main.getUserManager().getUser(player).getRank() >= 1) {
				 * PacketPlayOutOpenSignEditor packet = new PacketPlayOutOpenSignEditor( new
				 * BlockPosition(block.getX(), block.getY(), block.getZ())); ((CraftPlayer)
				 * player).getHandle().playerConnection.sendPacket(packet);
				 * 
				 * } }
				 */
			}

			// Check if player right clicked a chunk hopper
			/*
			 * CustomItem ci = main.getItemManager().getItem("chopper"); if (ci != null) {
			 * if (block.getType().toString().equalsIgnoreCase(ci.getMaterial())) { Location
			 * loc = block.getLocation(); if (main.getHopperManager().hasChunkHopper(loc)) {
			 * String world = loc.getWorld().getName(); String location = world + " " +
			 * loc.getBlockX() + " " + loc.getBlockY() + " " + loc.getBlockZ(); String coord
			 * = world + " " + loc.getChunk().getX() + " " + loc.getChunk().getZ(); if
			 * (main.getHopperManager().getChunkHopperLocation(coord).equalsIgnoreCase(
			 * location)) { util.debug("Clicked on Chunk Hopper"); //int id =
			 * main.getHopperManager().getChunkHopperId(coord);
			 * //main.getHopperManager().openChunkHopper(player, id);
			 * //event.setCancelled(true); } } } }
			 */
		}
	}

	@EventHandler
	public void onPlayerMove(PlayerMoveEvent event) {
		Player player = event.getPlayer();
		// Check for flight outside claimed area.
		if (player.getGameMode() == GameMode.SURVIVAL) {
			if (player.getAllowFlight()) {
				User user = main.getUserManager().getUser(player);
				int rank = user.getRank();
				if (rank >= 1) {
					Location loc = player.getLocation();
					String coord = main.getClaimManager().getChunkCoord(loc);
					if (main.getClaimManager().isClaimed(coord)) {
						String owner = main.getClaimManager().getClaimOwner(coord);
						if (!owner.equals(player.getName())) {
							player.setAllowFlight(false);
							player.setFlying(false);
						}
					} else {
						player.setAllowFlight(false);
						player.setFlying(false);
					}
				} else {
					player.setAllowFlight(false);
					player.setFlying(false);
				}
			}
		}

		if (player.getWorld().getName().contains("nether")) {
			if (event.getTo().getBlockY() >= 125) {
				if (event.getFrom().getBlockY() < 125) {
					event.setCancelled(true);
				} else {
					Location loc = event.getFrom();
					loc.setY(123);
					event.setTo(loc);
				}
			}
		}
	}

	@EventHandler
	public void onPlayerLevelChange(PlayerLevelChangeEvent event) {
		Player player = event.getPlayer();
		int oldlevel = event.getOldLevel();
		int newlevel = event.getNewLevel();
		User user = main.getUserManager().getUser(player);
		user.setLevel(newlevel);
	}
}
