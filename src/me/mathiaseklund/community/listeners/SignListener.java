package me.mathiaseklund.community.listeners;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;

import me.mathiaseklund.community.Main;
import me.mathiaseklund.community.users.User;
import me.mathiaseklund.community.utils.Util;

public class SignListener implements Listener {

	Main main = Main.getMain();
	Util util = main.getUtil();

	@EventHandler
	public void onSignChange(SignChangeEvent event) {
		Player player = event.getPlayer();
		User user = main.getUserManager().getUser(player);
		util.debug("SignChangeEvent");
		if (user.getRank() >= 1) {
			for (int i = 0; i < event.getLines().length; i++) {
				String line = event.getLines()[i];
				if (line != null) {
					line = ChatColor.translateAlternateColorCodes('&', line);
					event.setLine(i, line);
				}
			}
		}
	}
}
