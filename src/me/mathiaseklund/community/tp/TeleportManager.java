package me.mathiaseklund.community.tp;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import me.mathiaseklund.community.Main;
import me.mathiaseklund.community.users.User;
import me.mathiaseklund.community.utils.Util;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;

public class TeleportManager {

	Main main;
	Util util;

	HashMap<String, List<String>> tprequests = new HashMap<String, List<String>>();
	HashMap<String, Long> requestcd = new HashMap<String, Long>();

	public TeleportManager() {
		this.main = Main.getMain();
		util = main.getUtil();
	}

	// BOOLEANS

	public boolean hasOutgoingRequest(Player sender, Player target) {
		List<String> requests = getRequests(sender);
		return requests.contains(target.getName());
	}

	public boolean hasCooldown(Player sender) {
		boolean b = false;
		if (requestcd.containsKey(sender.getName())) {
			long cdend = requestcd.get(sender.getName());
			long now = Calendar.getInstance().getTimeInMillis();
			if (now < cdend) {
				b = true;
			} else {
				requestcd.remove(sender.getName());
			}
		}
		return b;
	}

	// GETTERS

	public List<String> getRequests(Player player) {
		List<String> requests = new ArrayList<String>();
		if (tprequests.containsKey(player.getName())) {
			requests = tprequests.get(player.getName());
		}
		return requests;

	}

	public long getCooldown(Player player) {
		long cd = 0;
		if (hasCooldown(player)) {
			cd = requestcd.get(player.getName());
		}
		return cd;
	}

	// SETTERS

	public void setRequests(Player player, List<String> requests) {
		tprequests.put(player.getName(), requests);
	}

	public void setCooldown(Player player) {
		long now = Calendar.getInstance().getTimeInMillis();
		long cdend = now;
		long cd1 = 120 * 1000;
		long cd2 = (60 * 60) * 1000;
		User user = main.getUserManager().getUser(player);
		if (user.getRank() >= 1) {
			cdend = cdend + cd1;
		} else {
			cdend = cdend + cd2;
		}
		requestcd.put(player.getName(), cdend);
	}

	// METHODS

	public void sendTeleportRequest(Player sender, Player target) {
		if (!sender.getName().equalsIgnoreCase(target.getName())) {
			if (!hasCooldown(sender)) {
				if (!hasOutgoingRequest(sender, target)) {
					List<String> requests = getRequests(sender);
					requests.add(target.getName());
					setRequests(sender, requests);
					util.message(sender, "&6Teleport Request sent to " + target.getName());
					util.message(target, "&6Teleport Request received from  " + sender.getName());
					util.jmessage(target, "&aWrite '/tpaccept " + sender.getName() + "' to accept request.",
							HoverEvent.Action.SHOW_TEXT, "&eClick to accept request", ClickEvent.Action.RUN_COMMAND,
							"/tpaccept " + sender.getName());
					Bukkit.getScheduler().runTaskLaterAsynchronously(main, new Runnable() {
						public void run() {
							List<String> requests = getRequests(sender);
							if (requests.contains(target.getName())) {
								requests.remove(target.getName());
								setRequests(sender, requests);
								util.message(sender, "&cTeleport Request to " + target.getName() + " timed out.");
								util.message(target, "&cTeleport Request from " + sender.getName() + " timed out.");
							}
						}
					}, 7 * 20);
				} else {
					util.message(sender, "&4ERROR:&7 There is already an outgoing request to this user.");
				}
			} else {
				long now = Calendar.getInstance().getTimeInMillis();
				long cdend = getCooldown(sender);
				if (cdend > 0) {
					long time = cdend - now;
					time = time / 1000;
					util.message(sender, "&4ERROR:&7 You can't request a teleport for: " + time + " Seconds");
				}
			}
		} else {
			util.message(sender, "&4ERROR:&7 You can't send teleport requests to yourself.");
		}
	}

	public void acceptTeleportRequest(Player target, Player sender) {
		util.debug(target.getName() + " wants to accept tp request from " + sender.getName());
		if (hasOutgoingRequest(sender, target)) {
			if (!hasCooldown(sender)) {
				List<String> requests = getRequests(sender);
				requests.remove(target.getName());
				setRequests(sender, requests);
				Bukkit.getScheduler().runTask(main, new Runnable() {
					public void run() {
						sender.teleport(target);
					}
				});
				util.message(sender, "&aYour teleport request was accepted by " + target.getName());
				util.message(target, "&aYou've accepted " + sender.getName() + "'s teleport request.");
				setCooldown(sender);
			}
		} else {
			util.message(target, "&4ERROR:&7 Target player has not requested to teleport to you.");
		}
	}

	public void denyTeleportRequest(Player target, Player sender) {
		util.debug(target.getName() + " wants to deny tp request from " + sender.getName());
		if (hasOutgoingRequest(sender, target)) {
			List<String> requests = getRequests(sender);
			requests.remove(target.getName());
			setRequests(sender, requests);
			util.message(sender, "&cYour teleport request was denied by " + target.getName());
			util.message(target, "&cYou've denied " + sender.getName() + "'s teleport request.");
		} else {
			util.message(target, "&4ERROR:&7 Target player has not requested to teleport to you.");
		}
	}
}
