package me.mathiaseklund.community.utils;

import java.text.NumberFormat;
import java.util.Locale;
import java.util.regex.Pattern;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.community.Main;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;

public class Util {

	Main main;

	public Util() {
		main = Main.getMain();
	}

	// METHODS

	public void message(CommandSender sender, String message) {
		if (sender != null) {
			if (message != null) {
				sender.sendMessage(ChatColor.translateAlternateColorCodes('&', message));
			}
		}
	}

	public void jmessage(CommandSender sender, String message, HoverEvent.Action heAction, String heText,
			ClickEvent.Action ceAction, String ceArg) {
		if (sender != null) {
			if (message != null) {
				TextComponent msg = new TextComponent(ChatColor.translateAlternateColorCodes('&', message));
				if (heAction != null) {
					if (heText != null) {
						msg.setHoverEvent(new HoverEvent(heAction,
								new ComponentBuilder(ChatColor.translateAlternateColorCodes('&', heText)).create()));
					}
				}
				if (ceAction != null) {
					if (ceArg != null) {
						msg.setClickEvent(new ClickEvent(ceAction, ceArg));
					}
				}
				sender.spigot().sendMessage(msg);
			}
		}
	}

	public void jmessage(CommandSender sender, String message, ClickEvent.Action ceAction, String ceArg) {
		if (sender != null) {
			if (message != null) {
				TextComponent msg = new TextComponent(ChatColor.translateAlternateColorCodes('&', message));
				if (ceAction != null) {
					if (ceArg != null) {
						msg.setClickEvent(new ClickEvent(ceAction, ceArg));
					}
				}
				sender.spigot().sendMessage(msg);
			}
		}
	}

	public void jmessage(CommandSender sender, String message, HoverEvent.Action heAction, String heText) {
		if (sender != null) {
			if (message != null) {
				TextComponent msg = new TextComponent(ChatColor.translateAlternateColorCodes('&', message));
				if (heAction != null) {
					if (heText != null) {
						msg.setHoverEvent(new HoverEvent(heAction,
								new ComponentBuilder(ChatColor.translateAlternateColorCodes('&', heText)).create()));
					}
				}
				sender.spigot().sendMessage(msg);
			}
		}
	}

	public boolean isInteger(String str) {
		if (str == null) {
			return false;
		}
		if (str.isEmpty()) {
			return false;
		}
		int i = 0;
		int length = str.length();
		if (str.charAt(0) == '-') {
			if (length == 1) {
				return false;
			}
			i = 1;
		}
		for (; i < length; i++) {
			char c = str.charAt(i);
			if (c < '0' || c > '9') {
				return false;
			}
		}
		return true;
	}

	public boolean isDouble(String str) {
		final Pattern DOUBLE_PATTERN = Pattern
				.compile("[\\x00-\\x20]*[+-]?(NaN|Infinity|((((\\p{Digit}+)(\\.)?((\\p{Digit}+)?)"
						+ "([eE][+-]?(\\p{Digit}+))?)|(\\.((\\p{Digit}+))([eE][+-]?(\\p{Digit}+))?)|"
						+ "(((0[xX](\\p{XDigit}+)(\\.)?)|(0[xX](\\p{XDigit}+)?(\\.)(\\p{XDigit}+)))"
						+ "[pP][+-]?(\\p{Digit}+)))[fFdD]?))[\\x00-\\x20]*");
		return DOUBLE_PATTERN.matcher(str).matches();
	}

	public void title(Player player, String title, String subtitle) {
		title(player, title, subtitle, 1);
	}

	public void title(Player player, String title, String subtitle, int time) {
		if (player != null) {
			if (title != null) {
				title = ChatColor.translateAlternateColorCodes('&', title);
			}

			if (subtitle != null) {
				subtitle = ChatColor.translateAlternateColorCodes('&', subtitle);
			}
			player.sendTitle(title, subtitle, 0, time * 20, 0);
		} else {
			error("Can't send title. Player is NULL");
		}
	}

	public String hide(String string) {
		StringBuilder builder = new StringBuilder();

		for (char c : string.toCharArray()) {
			builder.append(ChatColor.COLOR_CHAR).append(c);
		}
		return builder.toString();
	}

	public String unhide(String hidden) {
		if (hidden != null) {
			return hidden.replaceAll(String.valueOf(ChatColor.COLOR_CHAR), "");
		} else {
			return null;
		}
	}

	public void debug(String message) {
		if (message != null) {
			message = "[Community DEBUG] &7-&f " + message;
			Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', message));
		}

	}

	public void info(String message) {
		if (message != null) {
			message = "&a[Community INFO] &7-&f " + message;
			Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', message));
		}
	}

	public void warn(String message) {
		if (message != null) {
			message = "&e[Community WARN] &7-&f " + message;
			Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', message));
		}
	}

	public void error(String message) {
		if (message != null) {
			message = "&4[Community ERROR] &7-&c " + message;
			Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', message));
		}
	}

	public String formatMoney(double value) {
		String output = NumberFormat.getNumberInstance(Locale.US).format(value);
		return output;
	}

	public String capitalizeAllWords(String s) {
		String result = null;
		if (s.contains(" ")) {
			for (String str : s.split(" ")) {
				if (result == null) {
					result = str.substring(0, 1).toUpperCase() + str.substring(1);
				} else {
					result = result + " " + str.substring(0, 1).toUpperCase() + str.substring(1);
				}
			}
		} else {
			result = s.substring(0, 1).toUpperCase() + s.substring(1);
		}
		return result;
	}

	public Location convertStringToLocation(String s) {
		Location loc = null;
		if (s != null) {
			String[] strings = s.split(" ");
			World world;
			int x, y, z;
			float yaw, pitch;

			world = Bukkit.getWorld(strings[0]);
			x = Integer.parseInt(strings[1]);
			y = Integer.parseInt(strings[2]);
			z = Integer.parseInt(strings[3]);
			yaw = Float.parseFloat(strings[4]);
			pitch = Float.parseFloat(strings[5]);

			loc = new Location(world, x, y, z, yaw, pitch);
		}
		return loc;
	}

	public String convertLocationToString(Location loc) {
		String s = null;
		if (loc != null) {
			String world;
			int x, y, z;
			float yaw, pitch;

			world = loc.getWorld().getName();
			x = loc.getBlockX();
			y = loc.getBlockY();
			z = loc.getBlockZ();
			yaw = loc.getYaw();
			pitch = loc.getPitch();

			s = world + " " + x + " " + y + " " + z + " " + yaw + " " + pitch;
		}
		return s;
	}
}
