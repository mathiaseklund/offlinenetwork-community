package me.mathiaseklund.community;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;

import me.mathiaseklund.community.auction.AuctionHouse;
import me.mathiaseklund.community.claims.ClaimManager;
import me.mathiaseklund.community.commands.AuctionHouseCommand;
import me.mathiaseklund.community.commands.AutoPickupCommand;
import me.mathiaseklund.community.commands.BalanceCommand;
import me.mathiaseklund.community.commands.BanCommand;
import me.mathiaseklund.community.commands.BlockVaultCommand;
import me.mathiaseklund.community.commands.ClaimCommand;
import me.mathiaseklund.community.commands.CraftCommand;
import me.mathiaseklund.community.commands.DelHomeCommand;
import me.mathiaseklund.community.commands.FeedCommand;
import me.mathiaseklund.community.commands.FlyCommand;
import me.mathiaseklund.community.commands.GMCCommand;
import me.mathiaseklund.community.commands.GMSCommand;
import me.mathiaseklund.community.commands.HomeCommand;
import me.mathiaseklund.community.commands.InvseeCommand;
import me.mathiaseklund.community.commands.ItemCommand;
import me.mathiaseklund.community.commands.KickCommand;
import me.mathiaseklund.community.commands.LevelTopCommand;
import me.mathiaseklund.community.commands.PingCommand;
import me.mathiaseklund.community.commands.RepairCommand;
import me.mathiaseklund.community.commands.RespondCommand;
import me.mathiaseklund.community.commands.SellCommand;
import me.mathiaseklund.community.commands.SetHomeCommand;
import me.mathiaseklund.community.commands.SetRankCommand;
import me.mathiaseklund.community.commands.TPACommand;
import me.mathiaseklund.community.commands.TPAcceptCommand;
import me.mathiaseklund.community.commands.TPDenyCommand;
import me.mathiaseklund.community.commands.TrashCommand;
import me.mathiaseklund.community.commands.UnclaimCommand;
import me.mathiaseklund.community.commands.VanishCommand;
import me.mathiaseklund.community.commands.WhisperCommand;
import me.mathiaseklund.community.hoppers.HopperManager;
import me.mathiaseklund.community.items.ItemManager;
import me.mathiaseklund.community.listeners.BlockListener;
import me.mathiaseklund.community.listeners.ChatListener;
import me.mathiaseklund.community.listeners.CommandListener;
import me.mathiaseklund.community.listeners.EntityListener;
import me.mathiaseklund.community.listeners.InventoryListener;
import me.mathiaseklund.community.listeners.ItemListener;
import me.mathiaseklund.community.listeners.PlayerListener;
import me.mathiaseklund.community.listeners.SignListener;
import me.mathiaseklund.community.misc.MiscManager;
import me.mathiaseklund.community.mysql.MySQL;
import me.mathiaseklund.community.mysql.MySQLCommands;
import me.mathiaseklund.community.sell.SellManager;
import me.mathiaseklund.community.time.TimeManager;
import me.mathiaseklund.community.tp.TeleportManager;
import me.mathiaseklund.community.users.UserManager;
import me.mathiaseklund.community.utils.Util;

public class Main extends JavaPlugin {

	static Main main;

	UserManager um;
	ClaimManager cm;
	ItemManager im;
	HopperManager hm;
	MiscManager mm;
	SellManager sm;
	AuctionHouse ah;
	TimeManager tm;
	TeleportManager tp;
	Util util;

	public static Main getMain() {
		return main;
	}

	public void onEnable() {
		main = this;
		util = new Util();

		loadFiles();
		loadCommands();
		loadListeners();
		loadManagers();

		MySQL.connect();
		if (!MySQL.isConnected()) {
			util.error("MySQL Error. Disabled plugin");
			getServer().getPluginManager().disablePlugin(this);
		}

		for (Player p : Bukkit.getOnlinePlayers()) {
			getServer().getPluginManager().callEvent(new PlayerJoinEvent(p, null));
		}
	}

	public void onDisable() {
		if (MySQL.isConnected()) {
			MySQL.disconnect();
		}
	}

	void loadFiles() {
		File f = new File(getDataFolder(), "config.yml");
		if (!f.exists()) {
			saveResource("config.yml", true);
		}

		f = new File(getDataFolder() + "/users/");
		if (!f.exists()) {
			f.mkdir();
		}
		f = new File(getDataFolder() + "/hoppers/");
		if (!f.exists()) {
			f.mkdir();
		}

		f = new File(getDataFolder() + "/items/");
		if (!f.exists()) {
			f.mkdir();
			saveResource("items/chopper.yml", true);
		}

		f = new File(getDataFolder() + "/auctionhouse/");
		if (!f.exists()) {
			f.mkdir();
			saveResource("auctionhouse/auctionhouse.yml", true);
		}
	}

	void loadCommands() {
		getCommand("mysql").setExecutor(new MySQLCommands());
		getCommand("feed").setExecutor(new FeedCommand());
		getCommand("repair").setExecutor(new RepairCommand());
		getCommand("gms").setExecutor(new GMSCommand());
		getCommand("gmc").setExecutor(new GMCCommand());
		getCommand("claim").setExecutor(new ClaimCommand());
		getCommand("trash").setExecutor(new TrashCommand());
		getCommand("craft").setExecutor(new CraftCommand());
		getCommand("item").setExecutor(new ItemCommand());
		getCommand("balance").setExecutor(new BalanceCommand());
		getCommand("fly").setExecutor(new FlyCommand());
		getCommand("unclaim").setExecutor(new UnclaimCommand());
		getCommand("autopickup").setExecutor(new AutoPickupCommand());
		getCommand("vanish").setExecutor(new VanishCommand());
		getCommand("setrank").setExecutor(new SetRankCommand());
		getCommand("sell").setExecutor(new SellCommand());
		getCommand("whisper").setExecutor(new WhisperCommand());
		getCommand("respond").setExecutor(new RespondCommand());
		getCommand("auctionhouse").setExecutor(new AuctionHouseCommand());
		getCommand("tpa").setExecutor(new TPACommand());
		getCommand("tpaccept").setExecutor(new TPAcceptCommand());
		getCommand("tpdeny").setExecutor(new TPDenyCommand());
		getCommand("ping").setExecutor(new PingCommand());
		getCommand("ban").setExecutor(new BanCommand());
		getCommand("invsee").setExecutor(new InvseeCommand());
		getCommand("kick").setExecutor(new KickCommand());
		getCommand("home").setExecutor(new HomeCommand());
		getCommand("sethome").setExecutor(new SetHomeCommand());
		getCommand("delhome").setExecutor(new DelHomeCommand());
		getCommand("blockvault").setExecutor(new BlockVaultCommand());
		getCommand("lvltop").setExecutor(new LevelTopCommand());
	}

	void loadListeners() {
		getServer().getPluginManager().registerEvents(new PlayerListener(), this);
		getServer().getPluginManager().registerEvents(new EntityListener(), this);
		getServer().getPluginManager().registerEvents(new ChatListener(), this);
		getServer().getPluginManager().registerEvents(new BlockListener(), this);
		getServer().getPluginManager().registerEvents(new CommandListener(), this);
		getServer().getPluginManager().registerEvents(new InventoryListener(), this);
		getServer().getPluginManager().registerEvents(new ItemListener(), this);
		getServer().getPluginManager().registerEvents(new SignListener(), this);
	}

	void loadManagers() {
		um = new UserManager();
		cm = new ClaimManager();
		im = new ItemManager();
		hm = new HopperManager();
		mm = new MiscManager();
		sm = new SellManager();
		ah = new AuctionHouse();
		tm = new TimeManager();
		tp = new TeleportManager();
	}

	public UserManager getUserManager() {
		return um;
	}

	public ClaimManager getClaimManager() {
		return cm;
	}

	public ItemManager getItemManager() {
		return im;
	}

	public HopperManager getHopperManager() {
		return hm;
	}

	public MiscManager getMiscManager() {
		return mm;
	}

	public Util getUtil() {
		return util;
	}

	public SellManager getSellManager() {
		return sm;
	}

	public AuctionHouse getAuctionHouse() {
		return ah;
	}

	public TimeManager getTimeManager() {
		return tm;
	}

	public TeleportManager getTeleportManager() {
		return tp;
	}
}
