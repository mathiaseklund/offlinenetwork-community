package me.mathiaseklund.community.misc;

import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import me.mathiaseklund.community.Main;
import me.mathiaseklund.community.users.User;
import me.mathiaseklund.community.utils.Util;

public class MiscManager {

	Main main;
	Util util;

	public MiscManager() {
		main = Main.getMain();
		util = main.getUtil();
	}

	/*
	 * AUTO PICKUP
	 */

	// Locations for auto pickup, value is the player who broke the block.
	HashMap<String, String> aploc = new HashMap<String, String>();

	public boolean isAPLocation(Location loc) {
		String location = loc.getWorld().getName() + " " + loc.getBlockX() + " " + loc.getBlockY() + " "
				+ loc.getBlockZ();
		return isAPLocation(location);
	}

	public boolean isAPLocation(String location) {
		return aploc.containsKey(location);
	}

	public String getAPLocValue(Location loc) {
		String location = loc.getWorld().getName() + " " + loc.getBlockX() + " " + loc.getBlockY() + " "
				+ loc.getBlockZ();
		return getAPLocValue(location);
	}

	public String getAPLocValue(String location) {
		String value = null;
		if (isAPLocation(location)) {
			value = aploc.get(location);
		}
		return value;
	}

	public void setAPLocValue(Location loc, String player) {
		String location = loc.getWorld().getName() + " " + loc.getBlockX() + " " + loc.getBlockY() + " "
				+ loc.getBlockZ();
		setAPLocValue(location, player);
	}

	public void setAPLocValue(String key, String value) {
		if (value == null) {
			if (aploc.containsKey(key)) {
				aploc.remove(key);
			}
		} else {
			aploc.put(key, value);
		}
	}

	/*
	 * HOME & SETHOEM
	 */

	public void teleportHome(Player player, int homeIndex) {

		User user = main.getUserManager().getUser(player);
		int rank = user.getRank();
		boolean canTeleport = false;
		if (homeIndex > 1) {
			if (rank >= 1) {
				canTeleport = true;
			}
		} else {
			canTeleport = true;
		}
		if (canTeleport) {
			homeIndex--;
			String str = user.getHome(homeIndex);
			if (str != null) {
				Location loc = util.convertStringToLocation(str);
				util.message(player, "&6You will teleport in &c3 Seconds&6..");
				int hi = homeIndex;
				Bukkit.getScheduler().runTaskLater(main, new Runnable() {
					public void run() {
						player.teleport(loc);
						util.message(player, "&6You've been teleported to your Home &c#" + (hi + 1) + "&6!");
					}
				}, 3 * 20);

			} else {
				util.message(player, "&4ERROR:&7 Home not found.");
			}
		} else {
			util.message(player, "&4ERROR:&7 You are now allowed to access this home location!");
		}
	}

	public void setHome(Player player) {
		User user = main.getUserManager().getUser(player);
		List<String> list = user.getHomes();
		int homes = list.size();
		int rank = user.getRank();
		boolean canSet = true;
		if (homes > 0) {
			if (rank >= 1) {
				if (homes >= 5) {
					canSet = false;
				}
			} else {
				canSet = false;
			}
		}
		if (canSet) {
			String s = util.convertLocationToString(player.getLocation());
			user.addHome(s);
			homes = user.getHomes().size();
			util.message(player, "&6A new home has been set at your location! Home &c#" + homes);
		} else {
			util.message(player, "&4ERROR:&7 You are now allowed to store anymore home locations!");
		}
	}

	public void delHome(Player player, int homeIndex) {
		homeIndex--;
		User user = main.getUserManager().getUser(player);
		String home = user.getHome(homeIndex);
		if (home != null) {
			user.delHome(home);
			util.message(player, "&6You've deleted Home &c#" + (homeIndex + 1) + "&6!");
		} else {
			util.message(player, "&4ERROR:&7 Home not found.");
		}
	}

	/*
	 * BLOCK VAULTS
	 */

	public void openBlockVault(Player player, String mat) {
		User user = main.getUserManager().getUser(player);
		int amount = user.getBlockVaultAmount(mat);
		Inventory inv = Bukkit.createInventory(player, 54, util.hide("[bv]") + mat);
		ItemStack is = new ItemStack(Material.getMaterial(mat));
		if (amount > 3456) {
			is.setAmount(3456);
			amount = amount - 3456;
		} else {
			is.setAmount(amount);
			amount = 0;
		}
		user.setBlockVaultAmount(mat, amount);
		inv.addItem(is);
		Bukkit.getScheduler().runTask(main, new Runnable() {
			public void run() {
				player.openInventory(inv);
			}
		});

	}
}
