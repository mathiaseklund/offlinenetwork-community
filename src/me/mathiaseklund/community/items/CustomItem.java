package me.mathiaseklund.community.items;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataType;

import me.mathiaseklund.community.Main;
import me.mathiaseklund.community.utils.Util;

public class CustomItem {

	Main main;
	Util util;
	File f;
	FileConfiguration data;

	String id, material, displayname;
	List<String> lore, flags, enchants;
	byte matdata = -1;

	public CustomItem(String id, File f, FileConfiguration data) {
		this.main = Main.getMain();
		this.util = main.getUtil();
		this.id = id;
		this.f = f;
		this.data = data;
	}

	// GETTERS

	public String getId() {
		return id;
	}

	public String getMaterial() {
		if (material == null) {
			material = data.getString("material");
		}
		return material;
	}

	public String getDisplayName() {
		if (displayname == null) {
			displayname = data.getString("displayname");
		}
		return displayname;
	}

	public byte getMaterialData() {
		if (matdata < 0) {
			matdata = (byte) data.getInt("data");
		}
		return matdata;
	}

	public List<String> getLore() {
		if (lore == null) {
			lore = data.getStringList("lore");
		}
		return lore;
	}

	public List<String> getItemFlags() {
		if (flags == null) {
			flags = data.getStringList("flags");
		}
		return flags;
	}

	public List<String> getEnchants() {
		if (enchants == null) {
			enchants = data.getStringList("enchants");
		}
		return enchants;
	}

	public ItemStack getItemStack() {
		ItemStack is = null;

		String material = getMaterial();
		String name = getDisplayName();
		List<String> lore = getLore();
		List<String> flags = getItemFlags();
		List<String> enchants = getEnchants();

		if (material != null) {
			is = new ItemStack(Material.getMaterial(material), 1);

			if (enchants != null) {
				for (String s : enchants) {
					if (s.startsWith("mc:")) {
						String str = s.split("\\:")[1];
						int level = Integer.parseInt(str.split(" ")[1]);
						String ench = str.split(" ")[0];
						is.addUnsafeEnchantment(Enchantment.getByKey(NamespacedKey.minecraft(ench)), level);
					} else {
						// TODO custom enchants
					}
				}
			} else {
				util.warn("No enchantments found for item - " + id);
			}

			ItemMeta im = is.getItemMeta();
			im.getPersistentDataContainer().set(new NamespacedKey(main, "id"), PersistentDataType.STRING, getId());
			if (name != null) {
				im.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
			} else {
				util.warn("No displayname found for item - " + id);
			}

			if (lore != null) {
				List<String> list = new ArrayList<String>();
				for (String s : lore) {
					list.add(ChatColor.translateAlternateColorCodes('&', s));
				}
				im.setLore(list);
			} else {
				util.warn("No lore found for item - " + id);
			}

			if (flags != null) {
				for (String s : flags) {
					im.addItemFlags(ItemFlag.valueOf(s.toUpperCase()));
				}
			} else {
				util.warn("No flags found for item - " + id);
			}

			is.setItemMeta(im);
		} else {
			util.warn("No material found for item - " + id);
		}
		return is;
	}

	public ItemStack getItemStack(int amount) {
		ItemStack is = getItemStack();
		if (is != null) {
			is.setAmount(amount);
		}
		return is;
	}

}
