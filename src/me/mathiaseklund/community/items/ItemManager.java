package me.mathiaseklund.community.items;

import java.io.File;
import java.util.HashMap;

import org.bukkit.NamespacedKey;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;

import me.mathiaseklund.community.Main;
import me.mathiaseklund.community.utils.Util;

public class ItemManager {

	Main main;
	Util util;

	HashMap<String, CustomItem> items = new HashMap<String, CustomItem>();

	public ItemManager() {
		main = Main.getMain();
		util = main.getUtil();

		loadItems();
	}

	void loadItems() {
		util.info("Loading Custom Items");
		File file = new File(main.getDataFolder() + "/items/");
		if (file.exists()) {
			for (File f : file.listFiles()) {
				util.info("Found file: " + f.getName());
				FileConfiguration data = YamlConfiguration.loadConfiguration(f);

				String id = f.getName().replaceAll(".yml", "");
				CustomItem item = new CustomItem(id, f, data);
				items.put(id, item);
			}
		}
		util.info("Loaded all items");
	}

	// GETTERS

	public String getItemId(ItemStack is) {
		String id = null;
		if (is.hasItemMeta()) {
			ItemMeta im = is.getItemMeta();
			NamespacedKey key = new NamespacedKey(main, "id");
			PersistentDataContainer container = im.getPersistentDataContainer();
			if (container.has(key, PersistentDataType.STRING)) {
				id = container.get(key, PersistentDataType.STRING);
			}
		}
		return id;
	}

	public CustomItem getItem(String id) {
		if (itemExists(id)) {
			return items.get(id);
		} else {
			return null;
		}
	}

	public boolean itemExists(String id) {
		return items.containsKey(id);
	}
}
