package me.mathiaseklund.community.items;

import org.bukkit.craftbukkit.v1_14_R1.inventory.CraftItemStack;
import org.bukkit.inventory.ItemStack;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.minecraft.server.v1_14_R1.NBTTagCompound;

public class ItemTextAPI {

	public static TextComponent getItemText(ItemStack itemStack) {
		return getItemText(org.bukkit.craftbukkit.v1_14_R1.inventory.CraftItemStack.asNMSCopy(itemStack));
	}

	private static TextComponent getItemText(net.minecraft.server.v1_14_R1.ItemStack item) {
		return new TextComponent(item.save(new NBTTagCompound()).toString());
	}

	public static TextComponent getItemComponent(ItemStack item) {
		net.minecraft.server.v1_14_R1.ItemStack nmsStack = CraftItemStack.asNMSCopy(item);
		String name = null;
		if (nmsStack.getTag() != null && nmsStack.getTag().hasKeyOfType("display", 10)) {
			NBTTagCompound nbttagcompound = nmsStack.getTag().getCompound("display");
			if (nbttagcompound.hasKeyOfType("Name", 8)) {
				name = nbttagcompound.getString("Name");
			}
		}
		boolean named = name != null;
		if (!named) {
			name = nmsStack.getItem().getName();
		}
		TextComponent component = new TextComponent(
				TextComponent.fromLegacyText(ChatColor.translateAlternateColorCodes('&', "&8[&r")
						+ nmsStack.getName().getText() + ChatColor.translateAlternateColorCodes('&', "&8]&r")));
		for (int i = 0; i < component.getExtra().size(); i++) {
			BaseComponent baseExtra = component.getExtra().get(i);
			if (baseExtra.hasFormatting()) {
				break;
			}
			baseExtra.setColor(ChatColor.AQUA);
			if (named) {
				baseExtra.setItalic(true);
			}
		}
		component.setHoverEvent(getItemHover(nmsStack));
		return component;
	}

	private static HoverEvent getItemHover(net.minecraft.server.v1_14_R1.ItemStack nmsStack) {
		return new HoverEvent(HoverEvent.Action.SHOW_ITEM, new BaseComponent[] { getItemText(nmsStack) });
	}
}
