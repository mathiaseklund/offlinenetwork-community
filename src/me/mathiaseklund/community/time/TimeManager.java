package me.mathiaseklund.community.time;

import org.bukkit.Bukkit;
import org.bukkit.World;

import me.mathiaseklund.community.Main;
import me.mathiaseklund.community.utils.Util;

public class TimeManager {

	Main main;
	Util util;

	public TimeManager() {
		main = Main.getMain();
		util = main.getUtil();

		startCycle();
	}

	public void startCycle() {
		Bukkit.getScheduler().runTaskLater(main, new Runnable() {
			public void run() {
				for (World w : Bukkit.getWorlds()) {
					long time = w.getTime();
					time = time + 1000;
					w.setTime(time);

				}
				startCycle();
			}
		}, 150 * 20);
	}
}
