package me.mathiaseklund.community.auction;

import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.mathiaseklund.community.Main;
import me.mathiaseklund.community.mysql.MySQL;
import me.mathiaseklund.community.users.User;
import me.mathiaseklund.community.utils.Util;
import net.md_5.bungee.api.ChatColor;

public class AuctionHouse {

	Main main;
	Util util;
	String table = "auctionhouse";
	HashMap<Integer, Auction> auctions = new HashMap<Integer, Auction>();
	File f;
	FileConfiguration data;

	ArrayList<String> auctioning = new ArrayList<String>();

	public AuctionHouse() {
		main = Main.getMain();
		util = main.getUtil();
		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				loadAuctions();
			}
		});
	}

	void loadAuctions() {
		util.info("Loading open auctions.");
		f = new File(main.getDataFolder() + "/auctionhouse/auctions.yml");
		if (!f.exists()) {
			try {
				f.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		data = YamlConfiguration.loadConfiguration(f);
		ResultSet rs = MySQL.query(
				"SELECT id,finished,buyout,auctioneer,buyer FROM auctionhouse WHERE finished=0 ORDER BY buyout ASC;");
		try {
			while (rs.next()) {

				int id = rs.getInt("id");
				util.info("Found Auction: " + id);

				// Date started = rs.getDate("started");
				// Date ending = rs.getDate("ending");

				int finished = rs.getInt("finished");

				double buyout = rs.getDouble("buyout");

				String auctioneer = rs.getString("auctioneer");
				String buyer = rs.getString("buyer");

				ItemStack item = data.getItemStack(id + "");

				Auction auction = new Auction(id, finished, buyout, auctioneer, buyer, item);
				auctions.put(id, auction);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		util.info("Finished loading open auctions.");

	}

	void loadAuction(int id) {
		ResultSet rs = MySQL.query("SELECT finished,buyout,auctioneer,buyer FROM auctionhouse WHERE id=" + id);
		try {
			while (rs.next()) {
				util.info("Found Auction: " + id);

				// Date started = rs.getDate("started");
				// Date ending = rs.getDate("ending");

				int finished = rs.getInt("finished");

				double buyout = rs.getDouble("buyout");

				String auctioneer = rs.getString("auctioneer");
				String buyer = rs.getString("buyer");

				ItemStack item = data.getItemStack(id + "");

				Auction auction = new Auction(id, finished, buyout, auctioneer, buyer, item);
				auctions.put(id, auction);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	void saveItems() {
		try {
			data.save(f);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// AUCTION MENU

	public void openAuctionMenu(Player player, int page) {
		Inventory inv = Bukkit.createInventory(player, 54,
				util.hide("[ah]") + ChatColor.translateAlternateColorCodes('&', "&8Auction House #" + page));

		addFilterButton(inv, player);
		addOrderButton(inv, player);
		addListingsButton(inv, player);
		addInventoryButton(inv, player);
		addRefreshButton(inv);
		addNextButton(inv);
		addPreviousButton(inv);
		addFillerItems(inv);

		addAuctionItems(inv, player);

		player.openInventory(inv);
	}

	void addAuctionItems(Inventory inv, Player player) {
		// order by ascending price
		String order = "ORDER BY buyout ASC";
		String filter = "";
		ResultSet rs = MySQL.query("SELECT id FROM auctionhouse " + filter + order);
		try {
			while (rs.next()) {
				int id = rs.getInt(1);
				Auction auction = getAuction(id);
				if (auction != null) {
					if (auction.getFinished() < 1) {
						ItemStack is = new ItemStack(auction.getItemStack());
						if (is != null) {
							ItemMeta im = is.getItemMeta();
							String dname = null;
							List<String> oldlore = null;
							if (im.hasDisplayName()) {
								dname = im.getDisplayName();
							}
							if (im.hasLore()) {
								oldlore = im.getLore();
							}

							if (dname == null) {
								String s = is.getType().toString().toLowerCase();
								if (s.contains("_")) {
									s = s.replaceAll("_", " ");
								}
								s = util.capitalizeAllWords(s);
								dname = s;
							}
							String hidden = util.hide("[aid" + id + "]") + ChatColor.RESET;
							im.setDisplayName(hidden + dname);

							List<String> lore = new ArrayList<String>();
							if (oldlore != null) {
								lore.addAll(oldlore);
							}
							lore.add(ChatColor.translateAlternateColorCodes('&', ""));
							lore.add(ChatColor.translateAlternateColorCodes('&', "&eLeft-Click to Purchase Item"));
							lore.add(ChatColor.translateAlternateColorCodes('&',
									"&eAuction Price: &6$" + util.formatMoney(auction.getBuyout())));
							im.setLore(lore);

							is.setItemMeta(im);
							inv.addItem(is);
						}
					}
				} else {
					util.error("Unable to find auction with id: " + id);
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	void addFilterButton(Inventory inv, Player player) {
		ItemStack is = new ItemStack(Material.NETHER_STAR);
		ItemMeta im = is.getItemMeta();
		im.setDisplayName(util.hide("[filter]") + ChatColor.translateAlternateColorCodes('&', "&eFilter Settings"));
		is.setItemMeta(im);
		inv.setItem(4, is);
	}

	void addOrderButton(Inventory inv, Player player) {
		ItemStack is = new ItemStack(Material.REDSTONE);
		ItemMeta im = is.getItemMeta();
		im.setDisplayName(util.hide("[order]") + ChatColor.translateAlternateColorCodes('&', "&eOrder Settings"));
		is.setItemMeta(im);
		inv.setItem(5, is);
	}

	void addListingsButton(Inventory inv, Player player) {
		ItemStack is = new ItemStack(Material.CHEST);
		ItemMeta im = is.getItemMeta();
		im.setDisplayName(util.hide("[listings]") + ChatColor.translateAlternateColorCodes('&', "&eListings"));
		is.setItemMeta(im);
		inv.setItem(3, is);
	}

	void addInventoryButton(Inventory inv, Player player) {
		ItemStack is = new ItemStack(Material.ENDER_CHEST);
		ItemMeta im = is.getItemMeta();
		im.setDisplayName(util.hide("[inv]") + ChatColor.translateAlternateColorCodes('&', "&eInventory"));
		is.setItemMeta(im);
		inv.setItem(2, is);
	}

	void addRefreshButton(Inventory inv) {
		ItemStack is = new ItemStack(Material.DARK_OAK_BUTTON);
		ItemMeta im = is.getItemMeta();
		im.setDisplayName(util.hide("[refresh]") + ChatColor.translateAlternateColorCodes('&', "&eRefresh Listings"));
		is.setItemMeta(im);
		inv.setItem(6, is);
	}

	void addNextButton(Inventory inv) {
		ItemStack is = new ItemStack(Material.ARROW);
		ItemMeta im = is.getItemMeta();
		im.setDisplayName(util.hide("[next]") + ChatColor.translateAlternateColorCodes('&', "&eNext Page"));
		is.setItemMeta(im);
		inv.setItem(53, is);
	}

	void addPreviousButton(Inventory inv) {
		ItemStack is = new ItemStack(Material.ARROW);
		ItemMeta im = is.getItemMeta();
		im.setDisplayName(util.hide("[previous]") + ChatColor.translateAlternateColorCodes('&', "&ePrevious Page"));
		is.setItemMeta(im);
		inv.setItem(45, is);
	}

	void addFillerItems(Inventory inv) {
		ItemStack is = new ItemStack(Material.GRAY_STAINED_GLASS_PANE);
		ItemMeta im = is.getItemMeta();
		im.setDisplayName(util.hide("[filler]"));
		is.setItemMeta(im);
		// TODO set slots
		ArrayList<Integer> slots = new ArrayList<Integer>();
		slots.add(0);
		slots.add(1);
		slots.add(7);
		slots.add(8);
		slots.add(46);
		slots.add(47);
		slots.add(48);
		slots.add(49);
		slots.add(50);
		slots.add(51);
		slots.add(52);
		for (int i : slots) {
			inv.setItem(i, is);
		}
	}

	// AUCTION PROCESS

	public void startAuctionProcess(Player player) {
		ItemStack is = player.getInventory().getItemInMainHand();
		if (is != null && is.getType() != Material.AIR) {
			if (!auctioning.contains(player.getName())) {
				auctioning.add(player.getName());
				util.message(player, "&eStarted the auctioning process!");
				util.message(player, "&ePlease type your desired sell price in chat.");
				util.title(player, "&ePlease put item price in chat", "&cType 'cancel' to cancel process", 10);

			} else {
				util.message(player,
						"&4ERROR:&7 Already in the process of auctioning an item. Cancel auction by typing 'cancel'.");
			}
		} else {
			util.message(player, "&4ERROR: &7Need to hold an item in hand.");
		}
	}

	public void cancelAuctionProcess(Player player) {
		auctioning.remove(player.getName());
		util.message(player, "&cCancelled auction request.");
		util.title(player, "", "&cCancelled auction process");
	}

	public void finishAuctionProcess(Player player, double price) {
		auctioning.remove(player.getName());
		ItemStack is = new ItemStack(player.getInventory().getItemInMainHand());
		player.getInventory().setItemInMainHand(null);

		MySQL.update("INSERT INTO auctionhouse (finished, buyout, auctioneer) VALUES (0," + price + ",'"
				+ player.getName() + "');");
		ResultSet rs = MySQL.query("SELECT LAST_INSERT_ID();");
		try {
			while (rs.next()) {
				int id = rs.getInt(1);
				util.info("Created ID: " + id);
				updateItemStack(id, is);
				loadAuction(id);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		util.message(player, "&aSuccessfully put item up for auction!");
	}

	public void attemptPurchaseAuction(Player player, int id) {
		Auction auction = getAuction(id);
		User user = main.getUserManager().getUser(player);
		if (auction != null) {
			double price = auction.getBuyout();
			double balance = user.getBalance();
			if (balance >= price) {
				user.takeBalance(price);
				User auctioneer = main.getUserManager().getUser(auction.getAuctioneer());
				auctioneer.addBalance(price);
				auction.setBuyer(player.getName());
				auction.setFinished(1);
				auctions.remove(id);
				ItemStack is = new ItemStack(auction.getItemStack());
				// TODO add to auction inventory.
				player.getInventory().addItem(is);
				util.message(player,
						"&aYou've successfully purchased an item for &6$" + util.formatMoney(price) + "&a!");
				openAuctionMenu(player, 1);
				updateItemStack(id, null);
			} else {
				util.message(player, "&4ERROR:&7 Not enough money to purchase auction.");
			}
		} else {
			util.message(player, "&4ERROR:&7 Auction no longer exists.");
		}
	}

	// GETTERS

	public Auction getAuction(int id) {
		if (auctions.containsKey(id)) {
			return auctions.get(id);
		} else {
			return null;
		}
	}

	public boolean isAuctioning(Player player) {
		return isAuctioning(player.getName());
	}

	public boolean isAuctioning(String name) {
		return auctioning.contains(name);
	}

	// MODIFIERS

	public void updateItemStack(int id, ItemStack item) {
		data.set(id + "", item);
		saveItems();
	}
}
