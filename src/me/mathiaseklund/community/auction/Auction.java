package me.mathiaseklund.community.auction;

import java.sql.Date;

import org.bukkit.inventory.ItemStack;

import me.mathiaseklund.community.Main;
import me.mathiaseklund.community.mysql.SQL;
import me.mathiaseklund.community.utils.Util;

public class Auction {

	Main main;
	Util util;
	String table = "auctionhouse";
	int id, finished;
	Date started, ending;
	double buyout, startbid, lowestbidincr, currentbid;
	String auctioneer, buyer, currentbidder;
	ItemStack item;

	public Auction(int id, int finished, double buyout, String auctioneer, String buyer, ItemStack item) {
		this.id = id;
		main = Main.getMain();
		util = main.getUtil();

		this.item = item;
		this.finished = finished;
		this.buyout = buyout;
		this.auctioneer = auctioneer;
		this.buyer = buyer;
	}

	// GETTERS

	Date getStarted() {
		return started;
	}

	Date getEnding() {
		return ending;
	}

	int getFinished() {
		return finished;
	}

	int getId() {
		return id;
	}

	double getBuyout() {
		return buyout;
	}

	double getStartBid() {
		return startbid;
	}

	double getLowestBidIncr() {
		return lowestbidincr;
	}

	double getCurrentBid() {
		return currentbid;
	}

	String getAuctioneer() {
		return auctioneer;
	}

	String getBuyer() {
		return buyer;
	}

	String getCurrentBidder() {
		return currentbidder;
	}

	ItemStack getItemStack() {
		if (item == null) {
			util.error("Auction " + getId() + " | No ItemStack found.");
		}
		return item;
	}

	// SETTERS

	public void setFinished(int finished) {
		this.finished = finished;
		SQL.set("finished", finished, "id", "=", getId() + "", table);
	}

	public void setBuyout(double buyout) {
		this.buyout = buyout;
		SQL.set("buyout", buyout, "id", "=", getId() + "", table);
	}

	public void setStartBid(double startbid) {
		this.startbid = startbid;
		SQL.set("startbid", startbid, "id", "=", getId() + "", table);
	}

	public void setLowestBidIncr(double lowestbidincr) {
		this.lowestbidincr = lowestbidincr;
		SQL.set("lowestbidincr", lowestbidincr, "id", "=", getId() + "", table);
	}

	public void setCurrentBid(double currentbid) {
		this.currentbid = currentbid;
		SQL.set("currentbid", currentbid, "id", "=", getId() + "", table);
	}

	public void setAuctioneer(String auctioneer) {
		this.auctioneer = auctioneer;
		SQL.set("auctioneer", auctioneer, "id", "=", getId() + "", table);
	}

	public void setBuyer(String buyer) {
		this.buyer = buyer;
		SQL.set("buyer", buyer, "id", "=", getId() + "", table);
	}

	public void setCurrentBidder(String currentbidder) {
		this.currentbidder = currentbidder;
		SQL.set("currentbidder", currentbidder, "id", "=", getId() + "", table);
	}

	public void setItemStack(ItemStack item) {
		this.item = item;
		main.getAuctionHouse().updateItemStack(getId(), item);
	}
	// MODIFIERS
}
